@isTest
public with sharing class SkillsBasedRoutingTest {
    
    @isTest
    public static void testRouting() {
        Account a = new Account();
        a.Name = 'Test Account';
        a.Business_Unit__c = 'Quantabio';
        a.Account_Segment__c = 'Ag';
        a.RecordTypeId = '012U0000000MehKIAS';
        insert a;

        Contact cnt = new Contact();
        cnt.FirstName = 'Test';
        cnt.LastName = 'Name';
        cnt.AccountId = a.Id;
        insert cnt;
        
        // skill s = new skill();
        // s.DeveloperName = 'Other';
        // s.Description = 'Other';
        // s.MasterLabel = 'Other';
        // insert s;
 
        Case c= new Case();
        c.subject = 'test case';
        c.status = 'New';
        c.ContactId = cnt.Id;
        c.RecordTypeId = '0120B0000001w3sQAA';
        c.Request_Type__c = 'Other';
        insert c;

        List<String> casesToSend = new List<String>();
        casesToSend.add(c.id);
        SkillsBasedRouting.routeUsingSkills(casesToSend);

        // List<String> caseIdList = new List<String>();
        // caseIdList.add(c)
    }
}
