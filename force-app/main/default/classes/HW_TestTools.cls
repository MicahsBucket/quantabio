/**
* @author           Oliver Preuschl
*                   H&W Consult GmbH
*                   Bahnhofstr. 3
*                   21244 Buchholz i.d.N.
*                   http://www.hundw.com
*
* @description      Provides several tools that are often needed.
*
* @date             10.04.2015 
*
* Timeline:
* Name              DateTime                Version        Description
* Oliver Preuschl   10.04.2015                *1.0*          Created the class
*/

public class HW_TestTools{


    //Create a new record with pre populated required fields
    public static SObject createRecord( String PV_ObjectName ){
        sObject LO_Record = Schema.getGlobalDescribe().get( PV_ObjectName ).newSObject();
        Map< String, Schema.DescribeFieldResult > LM_RequiredFields = getRequiredFields( PV_ObjectName );
        setRequiredFields( LO_Record, LM_RequiredFields, 1 );
        return LO_Record;
    }

    //Get the required fields for the given object type
    public static Map< String, Schema.DescribeFieldResult > getRequiredFields( String PV_ObjectName ){
        Map< String, Schema.DescribeFieldResult > LM_RequiredFields = new Map< String, Schema.DescribeFieldResult >();
        Map< String, Schema.SObjectField > LM_ObjectFields = Schema.getGlobalDescribe().get( PV_ObjectName ).getDescribe().fields.getMap();
        for( String LV_FieldName: LM_ObjectFields.KeySet() ){
            Schema.SObjectField LO_Field = LM_ObjectFields.get( LV_FieldName );
            Schema.DescribeFieldResult LO_FieldDescribe = LO_Field.getDescribe();
            if ( ( !LO_FieldDescribe.isNillable() ) && ( !LO_FieldDescribe.isCalculated() ) && ( !LO_FieldDescribe.isAutonumber() ) && ( !LO_FieldDescribe.isDefaultedOnCreate() ) && ( !LO_FieldDescribe.isDeprecatedAndHidden() ) && ( LO_FieldDescribe.isUpdateable() ) ){
                LM_RequiredFields.put( LV_FieldName, LO_FieldDescribe );
            }
        }
        return LM_RequiredFields;
    }

    //Set the required fields for the given record
    public static void setRequiredFields( SObject PO_Record, Map< String, Schema.DescribeFieldResult > PM_RequiredFields, Integer PV_Number ){
        for( String LV_Field: PM_RequiredFields.KeySet() ){
            Schema.DescribeFieldResult LO_FieldDescribe = PM_RequiredFields.get( LV_Field );
            if ( LO_FieldDescribe.getType() == Schema.DisplayType.base64 ) {
                PO_Record.put( LV_Field, blob.valueOf( String.valueOf( PV_Number ) ) );
            } else if ( LO_FieldDescribe.getType() == Schema.DisplayType.Boolean ) {
                PO_Record.put( LV_Field, false );
            } else if ( LO_FieldDescribe.getType() == Schema.DisplayType.Combobox ) {
                PO_Record.put( LV_Field, String.valueOf( PV_Number ) );
            } else if ( LO_FieldDescribe.getType() == Schema.DisplayType.Currency ) {
                PO_Record.put( LV_Field, PV_Number );
            } else if ( LO_FieldDescribe.getType() == Schema.DisplayType.Date ) {
                PO_Record.put( LV_Field, Date.today() );
            } else if ( LO_FieldDescribe.getType() == Schema.DisplayType.DateTime ) {
                PO_Record.put( LV_Field, DateTime.now() );
            } else if ( LO_FieldDescribe.getType() == Schema.DisplayType.Double ) {
                PO_Record.put( LV_Field, PV_Number );
            } else if ( LO_FieldDescribe.getType() == Schema.DisplayType.Email ) {
                PO_Record.put( LV_Field, 'test' + String.valueOf( PV_Number ) + '@test.com' );
            } else if ( LO_FieldDescribe.getType() == Schema.DisplayType.EncryptedString ) {
                PO_Record.put( LV_Field, String.valueOf( PV_Number ) );
            } else if ( LO_FieldDescribe.getType() == Schema.DisplayType.Integer ) {
                PO_Record.put( LV_Field, PV_Number );
            } else if ( LO_FieldDescribe.getType() == Schema.DisplayType.MultiPicklist ) {
                PO_Record.put( LV_Field, String.valueOf( PV_Number ) );
            } else if ( LO_FieldDescribe.getType() == Schema.DisplayType.Percent ) {
                PO_Record.put(LV_Field, PV_Number );
            } else if ( LO_FieldDescribe.getType() == Schema.DisplayType.Phone ) {
                PO_Record.put( LV_Field, '123' );
            } else if ( LO_FieldDescribe.getType() == Schema.DisplayType.Picklist ) {
                PO_Record.put( LV_Field, String.valueOf( PV_Number ) );
            } else if ( LO_FieldDescribe.getType() == Schema.DisplayType.String ) {
                PO_Record.put( LV_Field, String.valueOf( PV_Number ) );
            } else if ( LO_FieldDescribe.getType() == Schema.DisplayType.TextArea ) {
                PO_Record.put( LV_Field, String.valueOf( PV_Number ) );
            } else if ( LO_FieldDescribe.getType() == Schema.DisplayType.Time ) {
                PO_Record.put( LV_Field, Time.newInstance( 0, 0, 0, 0 ) );
            } else if ( LO_FieldDescribe.getType() == Schema.DisplayType.URL ) {
                PO_Record.put( LV_Field, 'http://test' + String.valueOf( PV_Number ) + '.com' );
            } else {
                System.debug( 'Unhandled field type ' + LO_FieldDescribe.getType() );
            }
        }
    }

}