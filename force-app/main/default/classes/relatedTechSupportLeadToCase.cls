Global class relatedTechSupportLeadToCase {
    @InvocableMethod
    global static void createCase(List<Lead> leadList) {
        system.debug(leadList);
        Lead le = [SELECT Id, Country__c, Subject__c, FirstName, LastName, Email, Company, Case__c FROM Lead WHERE Id =: leadList[0].Id];
        system.debug(le);
        System.enqueueJob(new CreateCaseQueue(le));
    }
}
