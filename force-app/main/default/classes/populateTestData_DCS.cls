/**
*   {Purpose}  –  test class that loads data for other tests
*
*   {Function}  –  
*                  
*   {Support}   - For assistance with this code, please contact support@demandchainsystems.com              
*                 www.demandchainsystems.com
*                 (952) 345-4533
*/
/**
*   CHANGE  HISTORY
*   =============================================================================
*   Date      Name             Description
*   20181128  EBG DCS          Created
*   =============================================================================*/
@isTest
public class populateTestData_DCS {
    
    ////////////////////////////////////////////////////////////////////////////////
    //create Marketing Materials
    ////////////////////////////////////////////////////////////////////////////////
    public static list<Marketing_Material__c> createMarketingMaterial(Integer recordsToCreate) {
        List<Marketing_Material__c> lstRecords = new List<Marketing_Material__c>();
        for (Integer i=0; i<recordsToCreate; i++) {
            Marketing_Material__c oRecord = new Marketing_Material__c();
			oRecord.Name = 'Material ' + i;
            oRecord.Type__c = 'Give Away';
            oRecord.Active__c = true;
            oRecord.In_Stock_Beverly__c = 100;
            oRecord.In_Stock_Hilden__c = 100;
            lstRecords.add(oRecord);
        }
        insert lstRecords;
        return lstRecords;
    }

    ////////////////////////////////////////////////////////////////////////////////
    //create Marketing Kit
    ////////////////////////////////////////////////////////////////////////////////
    public static Marketing_Kit__c createMarketingKit(String location, List<Marketing_Material__c> lstMaterials) {
        Marketing_Kit__c oKit = new Marketing_Kit__c();
        oKit.Name = location + ' Kit';
        oKit.Location__c = location;

        insert oKit;
        //assign materials to the kit
        List<Marketing_Kit_Material__c> lstKitMaterials = new List<Marketing_Kit_Material__c>();
        for (Marketing_Material__c oMaterial : lstMaterials) {
            Marketing_Kit_Material__c oKitMaterial = new Marketing_Kit_Material__c();
            oKitMaterial.Name = oMaterial.Name;
            oKitMaterial.Marketing_Kit__c = oKit.Id;
            oKitMaterial.Marketing_Material__c = oMaterial.Id;
            oKitMaterial.Quantity__c = 1;
            lstKitMaterials.add(oKitMaterial);
        }
        insert lstKitMaterials;
        
        //return the created kit
        return oKit;
    }    
    
    ////////////////////////////////////////////////////////////////////////////////
    //create Material Order
    ////////////////////////////////////////////////////////////////////////////////
    public static Office_Order__c createMaterialOrder(Marketing_Kit__c oKit) {
        Office_Order__c oRecord = new Office_Order__c();
        oRecord.Name = 'Test Order';
        oRecord.Description__c = 'Description';
        oRecord.Marketing_Kit__c = oKit.Id;        

        insert oRecord;
        return oRecord;
    }

    
}