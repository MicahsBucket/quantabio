/**
* @author           Oliver Preuschl
*                   H&W Consult GmbH
*                   Bahnhofstr. 3
*                   21244 Buchholz i.d.N.
*                   http://www.hundw.com
*
* @description      Sending of the sample request order information emails based on the email type and the Country Assignment related to the opportunity
*
* date             11.03.2016
*
* Timeline:
* Name              DateTime                Version        Description
* Oliver Preuschl   11.03.2016              *1.0*          Created
*/

public without sharing class FivePrime_SampleRequestOrderEmailHandler {

    //The email template for each type
    private static Map< String, Id > GM_EmailType2EmailTemplateId {
        get {
            if( GM_EmailType2EmailTemplateId == null ) {
                GM_EmailType2EmailTemplateId = new Map< String, Id >();
                Set< String > LS_EmailTemplateNames = new Set< String >();
                for( SampleRequestEmailTemplate__mdt LO_EmailSetting: GM_EmailType2Setting.Values() ){
                    LS_EmailTemplateNames.add( LO_EmailSetting.EmailTemplateUniqueName__c );
                }
                Map< String, EmailTemplate > LM_EmailTemplates = new Map< String, EmailTemplate >();
                for( EmailTemplate LO_EmailTemplate: [ SELECT Id, DeveloperName FROM EmailTemplate WHERE ( DeveloperName IN :LS_EmailTemplateNames ) ] ){
                    LM_EmailTemplates.put( LO_EmailTemplate.DeveloperName, LO_EmailTemplate );
                }
                for( SampleRequestEmailTemplate__mdt LO_EmailSetting: GM_EmailType2Setting.Values() ){
                    if( LM_EmailTemplates.containsKey( LO_EmailSetting.EmailTemplateUniqueName__c ) ) {
                        String LV_EmailType = LO_EmailSetting.EmailType__c;
                        if( String.isNotBlank( LO_EmailSetting.Language__c ) ){
                            LV_EmailType += LO_EmailSetting.Language__c;
                        }
                        GM_EmailType2EmailTemplateId.put( LV_EmailType, LM_EmailTemplates.get( LO_EmailSetting.EmailTemplateUniqueName__c ).Id );
                    }
                }
            }
            return GM_EmailType2EmailTemplateId;
        }
        set;
    }

    //The email template names configured in the custom metadata types
    private static Map< String, SampleRequestEmailTemplate__mdt > GM_EmailType2Setting {
        get {
            if( GM_EmailType2Setting == null ) {
                GM_EmailType2Setting = new Map< String, SampleRequestEmailTemplate__mdt >();
                for( SampleRequestEmailTemplate__mdt LO_SampleRequestEmailTemplate: [ SELECT EmailType__c, Language__c, StatusField__c, EmailTemplateUniqueName__c, EmailRecipientCheckbox__c, OrgWideEmailAddressId__c FROM SampleRequestEmailTemplate__mdt ] ) {
                    String LV_EmailType = LO_SampleRequestEmailTemplate.EmailType__c;
                    if( String.isNotBlank( LO_SampleRequestEmailTemplate.Language__c ) ){
                        LV_EmailType += LO_SampleRequestEmailTemplate.Language__c;
                    }
                    GM_EmailType2Setting.put( LV_EmailType, LO_SampleRequestEmailTemplate );
                }
            }
            return GM_EmailType2Setting;
        }
        set;
    }

    //Sends the actual emails
    public static void sendEmails( Map< ID, Opportunity > PM_OldOpportunities, Map< Id, Opportunity > PM_NewOpportunities ) {
        Map< Id, Opportunity > LM_Opportunities = getRelevantOpportunities( PM_OldOpportunities, PM_NewOpportunities );
        Map< Id, QRep_Country_Assignement__c > LM_CountryAssignmentIds2CountryAssignments = getCountryAssignments( LM_Opportunities.Values() );
        Map< Id, Messaging.SingleEmailMessage > LM_OpportunityId2EmailMessage = new Map < Id, Messaging.SingleEmailMessage >();
        List< Messaging.SingleEmailMessage > LL_RawEmailMessages = new List < Messaging.SingleEmailMessage >();
        for( Opportunity LO_Opportunity: LM_Opportunities.Values() ) {
            if( LO_Opportunity.OpportunityContactRoles.size() > 0 ) {
                if( LO_Opportunity.Sample_Request_Type__c == 'Regular Sample Request' ) {
                    Id LV_EmailTemplateId = getEmailTemplateId( LO_Opportunity );
                    if( LV_EmailTemplateId != null ) {
                        List< String > LL_CCAddresses = getCCAddresses( LO_Opportunity, LM_CountryAssignmentIds2CountryAssignments.get( LO_Opportunity.QRep_Country_Assignment__c ) );
                        if( String.isNotBlank( LO_Opportunity.Owner.Email ) ){
                            LL_CCAddresses.add( LO_Opportunity.Owner.Email );
                        }
                        //if( String.isNotBlank( LO_Opportunity.Generalist__c ) && String.isNotBlank( LO_Opportunity.Generalist__r.Email ) ){
                            //LL_CCAddresses.add( LO_Opportunity.Generalist__r.Email );
                        //}
                        if( String.isNotBlank( LO_Opportunity.Generalist_Email__c ) ){
                            LL_CCAddresses.add( LO_Opportunity.Generalist_Email__c );    
                        }
                        //if( LO_Opportunity.OpportunityTeamMembers.size() > 0 && String.isNotBlank( LO_Opportunity.OpportunityTeamMembers.get( 0 ).User.Email ) ){
                            //LL_CCAddresses.add( LO_Opportunity.OpportunityTeamMembers.get( 0 ).User.Email );
                        //}
                        if( String.isNotBlank( LO_Opportunity.Requestor_Email_LSS__c ) ){
                            LL_CCAddresses.add( LO_Opportunity.Requestor_Email_LSS__c );    
                        }
                        List< String > LL_BCCAddresses = getBCCAddresses( LO_Opportunity, LM_CountryAssignmentIds2CountryAssignments.get( LO_Opportunity.QRep_Country_Assignment__c ) );
                        Messaging.SingleEmailMessage LO_Message = new Messaging.SingleEmailMessage();
                        String LV_OrgWideAddressId = getOrgWideAddressId( LO_Opportunity );
                        if( LV_OrgWideAddressId != null ){
                            LO_Message.setOrgWideEmailAddressId( LV_OrgWideAddressId );
                        }else{
                            if( LO_Opportunity.Lead_Creator__c != null && String.isNotBlank( LO_Opportunity.Lead_Creator__r.Name ) ) {
                                LO_Message.setSenderDisplayName( LO_Opportunity.Lead_Creator__r.Name );
                            }
                        }
                        LO_Message.setTemplateId( LV_EmailTemplateId );
                        LO_Message.setTargetObjectId( LO_Opportunity.OpportunityContactRoles.get( 0 ).ContactId );
                        LO_Message.setWhatId( LO_Opportunity.Id );
                        LO_Message.setCcAddresses( LL_CCAddresses );
                        LO_Message.setBccAddresses( LL_BCCAddresses );

                        //LO_Message.setTargetObjectId('0030U00000Pk1q5');
                        //LO_Message.setWhatId('0060U00000Dg5r2');

                        LM_OpportunityId2EmailMessage.put( LO_Opportunity.Id, LO_Message );
                        System.debug( 'TemplateId: ' + LV_EmailTemplateId );
                        System.debug( [ SELECT Id, Name FROM EmailTemplate WHERE ( Id = :LV_EmailTemplateId ) ] );
                        System.debug( 'TargetObjectId: ' + LO_Opportunity.OpportunityContactRoles.get( 0 ).ContactId );
                        System.debug( [ SELECT Id, Name FROM Contact WHERE ( Id = :LO_Opportunity.OpportunityContactRoles.get( 0 ).ContactId ) ] );
                        System.debug( 'WhatId: ' + LO_Opportunity.Id );
                        System.debug( [ SELECT Id, Name FROM Opportunity WHERE ( Id = :LO_Opportunity.Id ) ] );
                    }
                }
                if( ( getEmailType( LO_Opportunity ).startsWith( 'Confirmation' ) ) && ( ( LO_Opportunity.Sample_Request_Type__c == 'Short Sample Request' ) || ( LO_Opportunity.Sample_Request_Type__c == 'Regular Sample Request' ) ) ) {
                    if( GM_EmailType2EmailTemplateId.containsKey( 'Raw' ) ) {
                        Id LV_EmailTemplateId = GM_EmailType2EmailTemplateId.get( 'Raw' );
                        Id LV_TargetId = getRawTargetId( LM_CountryAssignmentIds2CountryAssignments.get( LO_Opportunity.QRep_Country_Assignment__c ) );
                        if( LV_TargetId != null ) {
                            List< String > LL_RawAddresses = getRawAddresses( LM_CountryAssignmentIds2CountryAssignments.get( LO_Opportunity.QRep_Country_Assignment__c ) );
                            LL_RawAddresses.remove( 0 );
                            if( String.isNotBlank( LO_Opportunity.Owner.Email ) ){
                                LL_RawAddresses.add( LO_Opportunity.Owner.Email );
                            }
                            //if( String.isNotBlank( LO_Opportunity.Generalist__c ) && String.isNotBlank( LO_Opportunity.Generalist__r.Email ) ){
                                //LL_RawAddresses.add( LO_Opportunity.Generalist__r.Email );
                            //}
                            if( String.isNotBlank( LO_Opportunity.Generalist_Email__c ) ){
                                LL_RawAddresses.add( LO_Opportunity.Generalist_Email__c );    
                            }
                            //if( LO_Opportunity.OpportunityTeamMembers.size() > 0 && String.isNotBlank( LO_Opportunity.OpportunityTeamMembers.get( 0 ).User.Email ) ){
                                //LL_RawAddresses.add( LO_Opportunity.OpportunityTeamMembers.get( 0 ).User.Email );
                            //}
                            if( String.isNotBlank( LO_Opportunity.Requestor_Email_LSS__c ) ){
                                LL_RawAddresses.add( LO_Opportunity.Requestor_Email_LSS__c );    
                            }
                            Messaging.SingleEmailMessage LO_Message = new Messaging.SingleEmailMessage();
                            if( LO_Opportunity.Lead_Creator__c != null && String.isNotBlank( LO_Opportunity.Lead_Creator__r.Name ) ) {
                                LO_Message.setSenderDisplayName( LO_Opportunity.Lead_Creator__r.Name );
                            }
                            LO_Message.setTemplateId( LV_EmailTemplateId );
                            LO_Message.setTargetObjectId( LV_TargetId );
                            LO_Message.setWhatId( LO_Opportunity.Id );
                            LO_Message.setToAddresses( LL_RawAddresses );
                            LL_RawEmailMessages.add( LO_Message );
                            System.debug( 'LL_RawEmailMessages: ' + LL_RawEmailMessages );
                        }
                    }
                    if( GM_EmailType2EmailTemplateId.containsKey( 'RawInternal' ) ) {
                        Id LV_EmailTemplateId = GM_EmailType2EmailTemplateId.get( 'RawInternal' );
                        Id LV_TargetId = getRawInternalTargetId( LM_CountryAssignmentIds2CountryAssignments.get( LO_Opportunity.QRep_Country_Assignment__c ) );
                        if( LV_TargetId != null ) {
                            List< String > LL_RawAddresses = getRawInternalAddresses( LM_CountryAssignmentIds2CountryAssignments.get( LO_Opportunity.QRep_Country_Assignment__c ) );
                            LL_RawAddresses.remove( 0 );
                            Messaging.SingleEmailMessage LO_Message = new Messaging.SingleEmailMessage();
                            if( LO_Opportunity.Lead_Creator__c != null && String.isNotBlank( LO_Opportunity.Lead_Creator__r.Name ) ) {
                                LO_Message.setSenderDisplayName( LO_Opportunity.Lead_Creator__r.Name );
                            }
                            LO_Message.setTemplateId( LV_EmailTemplateId );
                            LO_Message.setTargetObjectId( LV_TargetId );
                            LO_Message.setWhatId( LO_Opportunity.Id );
                            LO_Message.setToAddresses( LL_RawAddresses );
                            LL_RawEmailMessages.add( LO_Message );
                            System.debug( 'LL_RawEmailMessages: ' + LL_RawEmailMessages );
                        }
                    }
                }
            }
        }
        System.debug( 'LM_OpportunityId2EmailMessage: ' + LM_OpportunityId2EmailMessage );

        List< Messaging.SendEmailResult > LL_EmailResults;
        if(LM_OpportunityId2EmailMessage.size() > 0)
        {
            LL_EmailResults = Messaging.sendEmail( LM_OpportunityId2EmailMessage.Values() );
        }
        if(LL_RawEmailMessages.size() > 0)
        {
            Messaging.sendEmail( LL_RawEmailMessages );
        }
        updateOpportunityStatus( LM_Opportunities, LM_OpportunityId2EmailMessage, LL_EmailResults );
        resetOpportunityTrigger( LM_Opportunities );
    }

    //Get the email type for an specified opportunity
    private static String getEmailType( Opportunity PO_Opportunity ) {
        String LV_EmailType = PO_Opportunity.SR_Order_Email_Trigger__c;
        if( PO_Opportunity.QRep_Country_Assignment__c != null && String.isNotBlank( PO_Opportunity.QRep_Country_Assignment__r.Email_Language__c ) ){
            LV_EmailType += PO_Opportunity.QRep_Country_Assignment__r.Email_Language__c;
        }else{
            LV_EmailType += 'EN';
        }
        return LV_EmailType;
    }

    //Get the template id for a specified opportnity
    private static Id getEmailTemplateId( Opportunity PO_Opportunity ) {
        Id LV_EmailTemplateId;
        String LV_EmailType = getEmailType( PO_Opportunity );
        System.debug( 'Templates: ' + GM_EmailType2EmailTemplateId );
        System.debug( 'TemplateId for ' + LV_EmailType + ': ' + GM_EmailType2EmailTemplateId.get( LV_EmailType ) );
        if( GM_EmailType2EmailTemplateId.containsKey( LV_EmailType ) ) {
            LV_EmailTemplateId = GM_EmailType2EmailTemplateId.get( LV_EmailType );
        }
        System.debug('DC: LV_EmailTemplateId: ' + LV_EmailTemplateId);
        return LV_EmailTemplateId;
    }

    //Get the template id for a specified opportnity
    private static String getOrgWideAddressId( Opportunity PO_Opportunity ) {
        String LV_OrgWideAddressId = null;
        String LV_EmailType = getEmailType( PO_Opportunity );
        if( GM_EmailType2Setting.containsKey( LV_EmailType ) ) {
            SampleRequestEmailTemplate__mdt LO_EmailSettings = GM_EmailType2Setting.get( LV_EmailType );
            if( String.isNotBlank( LO_EmailSettings.OrgWideEmailAddressId__c ) ){
                LV_OrgWideAddressId = LO_EmailSettings.OrgWideEmailAddressId__c;
            }
        }
        return LV_OrgWideAddressId;
    }

    //Get the Raw Addresses for an opportunity
    private static List< String > getRawAddresses( QRep_Country_Assignement__c PO_CountryAssignment ) {
        List< String > LL_RawAddresses = new List< String >();
        if( PO_CountryAssignment != null ) {
            for( Email_Recepient__c LO_EmailRecipient: PO_CountryAssignment.Email_Recepients__r ) {
                if( LO_EmailRecipient.Email_Raw_Data__c ) {
                    LL_RawAddresses.add( LO_EmailRecipient.Contact__r.Email );
                }
            }
        }
        return LL_RawAddresses;
    }

    //Get the Raw Addresses for an opportunity
    private static List< String > getRawInternalAddresses( QRep_Country_Assignement__c PO_CountryAssignment ) {
        List< String > LL_RawAddresses = new List< String >();
        if( PO_CountryAssignment != null ) {
            for( Email_Recepient__c LO_EmailRecipient: PO_CountryAssignment.Email_Recepients__r ) {
                if( LO_EmailRecipient.Email_Raw_Data_Internal__c ) {
                    LL_RawAddresses.add( LO_EmailRecipient.Contact__r.Email );
                }
            }
        }
        return LL_RawAddresses;
    }

    //Get the Raw Addresses for an opportunity
    private static Id getRawTargetId( QRep_Country_Assignement__c PO_CountryAssignment ) {
        if( PO_CountryAssignment != null ) {
            for( Email_Recepient__c LO_EmailRecipient: PO_CountryAssignment.Email_Recepients__r ) {
                if( LO_EmailRecipient.Email_Raw_Data__c ) {
                    return LO_EmailRecipient.Contact__c;
                }
            }
        }
        return null;
    }

    //Get the Raw Addresses for an opportunity
    private static Id getRawInternalTargetId( QRep_Country_Assignement__c PO_CountryAssignment ) {
        if( PO_CountryAssignment != null ) {
            for( Email_Recepient__c LO_EmailRecipient: PO_CountryAssignment.Email_Recepients__r ) {
                if( LO_EmailRecipient.Email_Raw_Data_Internal__c ) {
                    return LO_EmailRecipient.Contact__c;
                }
            }
        }
        return null;
    }

    //Get the CC Addresses for an opportunity
    private static List< String > getCCAddresses( Opportunity PO_Opportunity, QRep_Country_Assignement__c PO_CountryAssignment ) {
        List< String > LL_CCAddresses = new List< String >();
        String LV_EmailType = getEmailType( PO_Opportunity );
        SampleRequestEmailTemplate__mdt LO_EmailSettings = GM_EmailType2Setting.get( LV_EmailType );
        if( PO_CountryAssignment != null ) {
            for( Email_Recepient__c LO_EmailRecipient: PO_CountryAssignment.Email_Recepients__r ) {
                if( LO_EmailRecipient.Email_CC__c && ( Boolean ) LO_EmailRecipient.get( LO_EmailSettings.EmailRecipientCheckbox__c ) ) {
                    LL_CCAddresses.add( LO_EmailRecipient.Contact__r.Email );
                }
            }
        }
        return LL_CCAddresses;
    }

    //Get the BCC Addresses for an opportunity
    private static List< String > getBCCAddresses( Opportunity PO_Opportunity, QRep_Country_Assignement__c PO_CountryAssignment ) {
        List< String > LL_BCCAddresses = new List< String >();
        String LV_EmailType = getEmailType( PO_Opportunity );
        SampleRequestEmailTemplate__mdt LO_EmailSettings = GM_EmailType2Setting.get( LV_EmailType );
        if( PO_CountryAssignment != null ) {
            for( Email_Recepient__c LO_EmailRecipient: PO_CountryAssignment.Email_Recepients__r ) {
                if( LO_EmailRecipient.Email_BCC__c && ( Boolean ) LO_EmailRecipient.get( LO_EmailSettings.EmailRecipientCheckbox__c ) ) {
                    LL_BCCAddresses.add( LO_EmailRecipient.Contact__r.Email );
                }
            }
        }
        return LL_BCCAddresses;
    }

    //Get those Opportunites that were updated to send an information email
    private static Map< Id, Opportunity > getRelevantOpportunities( Map< ID, Opportunity > PM_OldOpportunities, Map< Id, Opportunity > PM_NewOpportunities ) {
        Set< Id > LS_OpportunityIds = new Set< Id >();
        for( Id LV_OpportunityId: PM_NewOpportunities.KeySet() ) {
            Opportunity LO_OldOpportunity = PM_OldOpportunities.get( LV_OpportunityId );
            Opportunity LO_NewOpportunity = PM_NewOpportunities.get( LV_OpportunityId );
            if( ( LO_OldOpportunity.SR_Order_Email_Trigger__c != LO_NewOpportunity.SR_Order_Email_Trigger__c ) && String.isNotBlank( LO_NewOpportunity.SR_Order_Email_Trigger__c ) ) {
                LS_OpportunityIds.add( LO_NewOpportunity.Id );
            }
        }
        Map< Id, Opportunity > LM_Opportunities = new Map< Id, Opportunity >();
        LM_Opportunities = new Map< Id, Opportunity >( [
                SELECT  Sample_Request_Type__c,
                        SR_Order_Email_Trigger__c,
                        SR_Order_Confirmation_Email_Date__c,
                        SR_Order_1_Reminder_Email_Date__c,
                        SR_Order_2_Reminder_Email_Date__c,
                        SR_Order_3_Reminder_Email_Date__c,
                        SR_Order_ThankYou_Email_Date__c,
                        Owner.Email,
                        //Generalist__c,
                        //Generalist__r.Email,
                        Generalist_Email__c,
                        Requestor_Email_LSS__c,
                        Lead_Creator__r.Name,
                        Life_Science_Specialist__r.Email,
                        QRep_Country_Assignment__c,
                        QRep_Country_Assignment__r.Email_Language__c,
                        (
                            SELECT ContactId
                            FROM OpportunityContactRoles
                        ),
                        (
                                SELECT User.Email
                                FROM OpportunityTeamMembers
                        )
                FROM Opportunity
                WHERE ( Id IN :LS_OpportunityIds )
        ] );
        return LM_Opportunities;
    }

    //Get Country Assignments for specified Opportunities
    private static Map< Id, QRep_Country_Assignement__c > getCountryAssignments( List< Opportunity > PL_Opportunities ) {
        Set< Id > LS_CountryAssignmentIds = new Set< Id >();
        for( Opportunity LO_Opportunity: PL_Opportunities ) {
            LS_CountryAssignmentIds.add( LO_Opportunity.QRep_Country_Assignment__c );
        }
        Map< Id, QRep_Country_Assignement__c > LM_CountryAssignmentIds2CountryAssignments = new Map< Id, QRep_Country_Assignement__c >( [
                SELECT
                        Id, (
                        SELECT
                                Contact__c,
                                Contact__r.Email,
                                Email_cc__c,
                                Email_Bcc__c,
                                Email_Evaluation_Reagents__c,
                                Email_Evaluation_Completion_Reminder__c,
                                Email_Thank_you__c,
                                Email_Raw_Data__c,
                                Email_Raw_Data_Internal__c
                        FROM Email_Recepients__r
                )
                FROM QRep_Country_Assignement__c
                WHERE ( Id IN :LS_CountryAssignmentIds )
        ] );
        return LM_CountryAssignmentIds2CountryAssignments;
    }

    //Resets the Trigger Field on the Opportunities
    private static void resetOpportunityTrigger( Map< Id, Opportunity > PM_Opportunities ) {
        List< Opportunity > LL_Opportunties_Update = new List< Opportunity >();
        for( Id LV_OpportunityId: PM_Opportunities.KeySet() ) {
            Opportunity LO_Opportunity = PM_Opportunities.get( LV_OpportunityId );
            Opportunity LO_Opportunity_Update = new Opportunity(
                    Id = LV_OpportunityId,
                    SR_Order_Email_Trigger__c = ''
            );
            LL_Opportunties_Update.add( LO_Opportunity_Update );
        }
        update( LL_Opportunties_Update );
    }


    //Updates the Email Status on the Opportunities
    private static void updateOpportunityStatus( Map< Id, Opportunity > PM_Opportunities, Map< Id, Messaging.SingleEmailMessage > PM_OpportunityId2EmailMessage, List< Messaging.SendEmailResult > PL_EmailResults ) {
        List< Opportunity > LL_Opportunties_Update = new List< Opportunity >();
        Integer i = 0;
        for( Id LV_OpportunityId: PM_OpportunityId2EmailMessage.KeySet() ) {
            Opportunity LO_Opportunity = PM_Opportunities.get( LV_OpportunityId );
            String LV_EmailType = getEmailType( LO_Opportunity );
            String LV_EmailStatusField = GM_EmailType2Setting.get( LV_EmailType ).StatusField__c;
            Messaging.SendEmailResult LO_EmailResult = PL_EmailResults.get( i );
            Opportunity LO_Opportunity_Update = new Opportunity(
                    Id = LV_OpportunityId,
                    SR_Order_Email_Trigger__c = '',
                    SR_Order_Email_Error__c = ''
            );
            if( LO_EmailResult.isSuccess() ) {
                LO_Opportunity_Update.put( LV_EmailStatusField, Date.today() );
            } else {
                //LO_Opportunity_Update.put( LV_EmailStatusField, 'Error' );
                String LV_ErrorString = '';
                for( Messaging.SendEmailError LO_EmailError: LO_EmailResult.getErrors() ) {
                    LV_ErrorString += LO_EmailError.getMessage() + '\n';
                }
                LO_Opportunity.SR_Order_Email_Error__c = LV_ErrorString;
            }
            LL_Opportunties_Update.add( LO_Opportunity_Update );
            i++;
        }
        update( LL_Opportunties_Update );
    }

}