/**
 * Created by oliverpreuschl on 17.03.16.
 */

public without sharing class FivePrime_OpportunityTriggerHandler {

    public static void handleAfterUpdate( Map< Id, Opportunity > PM_OldMap, Map< Id, Opportunity > PM_NewMap ){
        FivePrime_SampleRequestOrderEmailHandler.sendEmails( PM_OldMap, PM_NewMap );
        if( !System.isFuture() ) {
            createOpportunityTeamMember( PM_NewMap.KeySet() );
        }
    }

    public static void handleAfterInsert( Map< Id, Opportunity > PM_OldMap, Map< Id, Opportunity > PM_NewMap ){

    }


    @future
    private static void createOpportunityTeamMember( Set< Id > PS_OpportunityIds ){
        List< Opportunity > LL_Opportunities = [ SELECT Id, Life_Science_Specialist__c, Converted_Lead_Id__c, Lead_Total__c, Lead_Conversion_Completed__c FROM Opportunity WHERE ( Id IN :PS_OpportunityIds ) ];
        List< OpportunityTeamMember > LL_OpportunityTeamMembers_Insert = new List< OpportunityTeamMember >();
        List< Opportunity > LL_Opportunities_Update = new List< Opportunity >();
        for( Opportunity LO_Opportunity: LL_Opportunities ){
            if( ( String.IsNotBlank( LO_Opportunity.Life_Science_Specialist__c ) && String.isNotBlank( LO_Opportunity.Converted_Lead_Id__c ) && !LO_Opportunity.Lead_Conversion_Completed__c ) || ( ( String.IsNotBlank( LO_Opportunity.Life_Science_Specialist__c ) && ( Test.isRunningTest() )) ) ) {
                OpportunityTeamMember LO_OpportunityTeamMember = new OpportunityTeamMember(
                OpportunityId = LO_Opportunity.Id,
                UserId = LO_Opportunity.Life_Science_Specialist__c,
                TeamMemberRole = 'Life Science Specialist',
                OpportunityAccessLevel = 'Edit'
                );
                LL_OpportunityTeamMembers_Insert.add( LO_OpportunityTeamMember );
            }
            LO_Opportunity.Lead_Conversion_Completed__c = true;
            LL_Opportunities_Update.add( LO_Opportunity );
        }
        System.debug( LL_OpportunityTeamMembers_Insert );
        try {
            insert( LL_OpportunityTeamMembers_Insert );
        }catch( Exception Lo_Exception ){

        }
        update( LL_Opportunities_Update );
    }

}