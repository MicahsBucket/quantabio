/**
* @author           oliverpreuschl
*                   H&W Consult GmbH
*                   Bahnhofstr. 3
*                   21244 Buchholz i.d.N.
*                   http://www.hundw.com
*
* @description      
*
* @date             18.08.17 
*
* Timeline:
* Name              Date                Version        Description
* oliverpreuschl           18.08.17             *1.0*          Created the class
*/

@IsTest
private class FivePrime_SampleRequestConfirmationC_T {

    static testMethod void testConfirmation_Confirmed() {
        Test.startTest();

        Id LV_LeadId = createLead();
        Lead LO_Lead = [ SELECT Sample_Request_Status__c FROM Lead WHERE ( Id = :LV_LeadId ) ];
        System.assertEquals( LO_Lead.Sample_Request_Status__c, 'Customer confirmation requested' );

        ApexPages.StandardController LO_StandardController = new ApexPages.StandardController( LO_Lead );
        FivePrime_SampleRequestConfirmationC LO_Controller = new FivePrime_SampleRequestConfirmationC( LO_StandardController );
        Test.setCurrentPage( Page.FivePrime_SampleRequestConfirmation );
        ApexPages.currentPage().getParameters().put( 'id', LV_LeadId );
        ApexPages.currentPage().getParameters().put( 'response', 'Confirmed' );
        System.assertEquals( false, LO_Controller.GV_Error );
        System.assertEquals( true, LO_Controller.GV_ShowComment );
        LO_Controller.saveResponse();
        System.assertEquals( true, LO_Controller.GV_Success_1 );
        System.assertEquals( '', LO_Controller.GV_CustomerComment );
        LO_Controller.GV_CustomerComment = 'Test-Comment';
        System.assertEquals( true, LO_Controller.GV_ShowComment );
        LO_Controller.saveComment();
        System.assertEquals( true, LO_Controller.GV_Success_2 );
        System.assertEquals( false, LO_Controller.GV_ShowComment );

        Test.stopTest();

        LO_Lead = [ SELECT Sample_Request_Status__c FROM Lead WHERE ( Id = :LV_LeadId ) ];
        System.assertEquals( LO_Lead.Sample_Request_Status__c, 'Confirmed' );

    }

    static testMethod void testConfirmation_Declined() {
        Test.startTest();

        Id LV_LeadId = createLead();
        Lead LO_Lead = [ SELECT Sample_Request_Status__c FROM Lead WHERE ( Id = :LV_LeadId ) ];
        System.assertEquals( LO_Lead.Sample_Request_Status__c, 'Customer confirmation requested' );

        ApexPages.StandardController LO_StandardController = new ApexPages.StandardController( LO_Lead );
        FivePrime_SampleRequestConfirmationC LO_Controller = new FivePrime_SampleRequestConfirmationC( LO_StandardController );
        Test.setCurrentPage( Page.FivePrime_SampleRequestConfirmation );
        ApexPages.currentPage().getParameters().put( 'id', LV_LeadId );
        ApexPages.currentPage().getParameters().put( 'response', 'Declined' );
        LO_Controller.saveResponse();

        Test.stopTest();

        LO_Lead = [ SELECT Sample_Request_Status__c FROM Lead WHERE ( Id = :LV_LeadId ) ];
        System.assertEquals( LO_Lead.Sample_Request_Status__c, 'Declined' );

    }

    static testMethod void testConfirmation_Call() {
        Test.startTest();

        Id LV_LeadId = createLead();
        Lead LO_Lead = [ SELECT Sample_Request_Status__c FROM Lead WHERE ( Id = :LV_LeadId ) ];
        System.assertEquals( LO_Lead.Sample_Request_Status__c, 'Customer confirmation requested' );

        ApexPages.StandardController LO_StandardController = new ApexPages.StandardController( LO_Lead );
        FivePrime_SampleRequestConfirmationC LO_Controller = new FivePrime_SampleRequestConfirmationC( LO_StandardController );
        Test.setCurrentPage( Page.FivePrime_SampleRequestConfirmation );
        ApexPages.currentPage().getParameters().put( 'id', LV_LeadId );
        ApexPages.currentPage().getParameters().put( 'response', 'Call' );
        LO_Controller.saveResponse();

        Test.stopTest();

        LO_Lead = [ SELECT Sample_Request_Status__c FROM Lead WHERE ( Id = :LV_LeadId ) ];
        System.assertEquals( LO_Lead.Sample_Request_Status__c, 'Call' );

    }

    static Id createLead() {
        QRep_Country_Assignement__c LO_CountryAssignment = new QRep_Country_Assignement__c(
                Email_Language__c = 'EN'
        );
        insert( LO_CountryAssignment );

        Lead LO_Lead = new Lead(
                RecordTypeId                            = SM_RecordTypeName2RecordType.get( 'Sample_Request' ),
                LeadSource                              = 'Sample Request',
                Sample_Request_Status__c                = 'Customer confirmation requested',
                QRep_Country_Assignment__c              = LO_CountryAssignment.Id,
                SampleProduct_Information_Complete__c   = true,
                CountryCode                             = 'de',
                LastName                                = 'LastName',
                Company                                 = 'Company',
                Kind_of_Convert__c                      = 'Regular Sample Request',
                Email                                   = 'test@hundw.com',
            	Generalist_Email__c						= 'test@invalid.com',
                Requestor_Email_LSS__c					= 'test@invalid.com'
        );
        insert( LO_Lead );
        return LO_Lead.Id;
    }

    private static Map< String, Id > SM_RecordTypeName2RecordType {
        get {
            if( SM_RecordTypeName2RecordType == null ) {
                SM_RecordTypeName2RecordType = new Map< String, Id >();
                for( RecordType LO_RecordType : [ Select Id, DeveloperName From RecordType Where ( SObjectType = 'Lead' ) ] ) {
                    SM_RecordTypeName2RecordType.put( LO_RecordType.DeveloperName, LO_RecordType.Id );
                }
            }
            System.debug( SM_RecordTypeName2RecordType );
            return SM_RecordTypeName2RecordType;
        }
        set;
    }

}