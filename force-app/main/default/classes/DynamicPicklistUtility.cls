public without sharing class DynamicPicklistUtility {

    @AuraEnabled
    public static List<SelectOptionObj> getStates() { 
        System.debug('In Get State');
        List<SelectOptionObj> picklistOptions = new List<SelectOptionObj>();
        Schema.SObjectType obj_describe = Schema.getGlobalDescribe().get('Lead') ;
        Schema.DescribeSObjectResult obj_describe_result = obj_describe.getDescribe() ;
        Map<String,Schema.SObjectField> fields = obj_describe_result.fields.getMap() ;
        Schema.DescribeFieldResult fieldResult = fields.get('State_Picklist__c').getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        SelectOptionObj option = null;
        for (Schema.PicklistEntry pickListVal : ple) {
            option = new SelectOptionObj(pickListVal.getLabel(), pickListVal.getValue(), pickListVal.isDefaultValue());
            picklistOptions.add(option);
        }
        return picklistOptions;
    }
    @AuraEnabled
    public static List<SelectOptionObj> getProvinces() {
        System.debug('In Get Province');
        List<SelectOptionObj> picklistOptions = new List<SelectOptionObj>();
        Schema.SObjectType obj_describe = Schema.getGlobalDescribe().get('Lead') ;
        Schema.DescribeSObjectResult obj_describe_result = obj_describe.getDescribe() ;
        Map<String,Schema.SObjectField> fields = obj_describe_result.fields.getMap() ;
        Schema.DescribeFieldResult fieldResult = fields.get('Province_Picklist__c').getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        SelectOptionObj option = null;
        for (Schema.PicklistEntry pickListVal : ple) {
            option = new SelectOptionObj(pickListVal.getLabel(), pickListVal.getValue(), pickListVal.isDefaultValue());
            picklistOptions.add(option);
        }
        return picklistOptions;
    }

    public class SelectOptionObj {
        @AuraEnabled
        public String label { get; set; }
        @AuraEnabled
        public String value { get; set; }
        @AuraEnabled
        public Boolean isDefault {get; set;}
        
        public SelectOptionObj(String label, String val,Boolean isDefault) {
            this.label = label;
            this.value = val;
            this.isDefault = isDefault;
        }
    }
}
