/**
*   {Purpose}  –  test class for the PopulateTestData_DC class
*
*   {Function}  –
*
*   {Support}   - For assistance with this code, please contact support@demandchainsystems.com
*                 www.demandchainsystems.com
*                 (952) 345-4533
*/
/**
*   CHANGE  HISTORY
*   =============================================================================
*   Date      Name             Description
*   20180118  CAM DCS          Created
*   =============================================================================*/
@isTest 
Private class testPopulateTestData_DC_Methods {
    @isTest static void TestLead_OSP_Count () {
        list<Lead> lstLead = PopulateTestData_DC.createLead(1);
        list<Product2> lstProduct2 = populateTestData_DC.createProduct(1);
        Ordered_Sample_Product__c OSP = populateTestData_DC.createOSP(lstLead[0].id, lstProduct2[0].id);
        delete OSP;
    }
}