/**
 * Created by oliverpreuschl on 09.06.16.
 */

@IsTest
private class FivePrime_SampleRequestSurveyTest {

    static testMethod void testSurveySave() {
        Opportunity LO_Opportunity = createTestData();

        Test.startTest();

        Test.setCurrentPage( Page.FivePrime_Survey );
        ApexPages.currentPage().getParameters().put( 't', '2' );
        ApexPages.StandardController LO_StandardController = new ApexPages.StandardController( LO_Opportunity );
        FivePrime_SampleRequestSurveyController LO_SurveyController = new FivePrime_SampleRequestSurveyController( LO_StandardController );
        System.assertEquals( 2, LO_SurveyController.GV_IncentiveType );
        System.assertEquals( 'ContactName', LO_SurveyController.GV_ContactName );

        LO_SurveyController.GV_Sensitivity = 'Improved';
        LO_SurveyController.GV_Reproducibility = 'Improved';
        LO_SurveyController.GV_Specificity = 'Imrpoved';
        LO_SurveyController.GV_EaseOfUse = 'Imrpoved';
        LO_SurveyController.GV_Overall = 'Imrpoved';
        LO_SurveyController.GV_EvaluationComment = 'Comment';
        LO_SurveyController.GV_Recommendation = 'Yes';
        LO_SurveyController.GV_CurrentCustomer = 'Yes';
        LO_SurveyController.GV_Incentive = '2for3';

        LO_SurveyController.saveSurvey();

        Test.stopTest();

        LO_Opportunity = [
                SELECT Sensitivity__c,
                        Reproducibility__c,
                        Specificity__c,
                        Ease_of_use__c,
                        Overall__c,
                        Evaluation_Comment__c,
                        Recommendation__c,
                        Current_Customer__c,
                        Gift_Choice__c
                FROM Opportunity
        ];

        System.assertEquals( LO_SurveyController.GV_Sensitivity, LO_Opportunity.Sensitivity__c );
        System.assertEquals( LO_SurveyController.GV_Reproducibility, LO_Opportunity.Reproducibility__c );
        System.assertEquals( LO_SurveyController.GV_Specificity, LO_Opportunity.Specificity__c );
        System.assertEquals( LO_SurveyController.GV_EaseOfUse, LO_Opportunity.Ease_of_use__c );
        System.assertEquals( LO_SurveyController.GV_Overall, LO_Opportunity.Overall__c );
        System.assertEquals( LO_SurveyController.GV_EvaluationComment, LO_Opportunity.Evaluation_Comment__c );
        System.assertEquals( LO_SurveyController.GV_Recommendation, LO_Opportunity.Recommendation__c );
        System.assertEquals( LO_SurveyController.GV_CurrentCustomer, LO_Opportunity.Current_Customer__c );
        System.assertEquals( LO_SurveyController.GV_Incentive, LO_Opportunity.Gift_Choice__c );
    }

    static Opportunity createTestData() {
        Account LO_Account = ( Account ) HW_TestTools.createRecord( 'Account' );
        insert( LO_Account );

        Contact LO_Contact = ( Contact ) HW_TestTools.createRecord( 'Contact' );
        LO_Contact.AccountId = LO_Account.Id;
        LO_Contact.LastName = 'ContactName';
        LO_Contact.Email = 'test@hundw.com';
        insert( LO_Contact );

        Opportunity LO_Opportunity = ( Opportunity ) HW_TestTools.createRecord( 'Opportunity' );
        LO_Opportunity.AccountId = LO_Account.Id;
        LO_Opportunity.Sample_Request_Type__c = 'Regular Sample Request';
        LO_Opportunity.Lead_Creator__c = UserInfo.getUserId();
        insert( LO_Opportunity );

        OpportunityContactRole LO_OpportunityContactRole = ( OpportunityContactRole ) HW_TestTools.createRecord( 'OpportunityContactRole' );
        LO_OpportunityContactRole.OpportunityId = LO_Opportunity.Id;
        LO_OpportunityContactRole.ContactId = LO_Contact.Id;
        insert( LO_OpportunityContactRole );

        return LO_Opportunity;
    }

}