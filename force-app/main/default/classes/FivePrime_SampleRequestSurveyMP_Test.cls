@IsTest
public class FivePrime_SampleRequestSurveyMP_Test  {
/**
 * This class derrived from "FivePrime_SampleRequestSurveyTest" Created by oliverpreuschl on 25.05.16.
 * Modified by Brian Tremblay of Demand Chain
 *		Modified on 7/24/2018
 *		Modified to support Multiple Opportunity Products
 */

    static testMethod void testSurveySave() {
        Opportunity LO_Opportunity = createTestData();

        Test.startTest();

        Test.setCurrentPage( Page.FivePrime_Survey );
        ApexPages.currentPage().getParameters().put( 't', '2' );
        ApexPages.StandardController LO_StandardController = new ApexPages.StandardController( LO_Opportunity );
        FivePrime_SampleRequestSurveyMP_Ctlr LO_SurveyController = new FivePrime_SampleRequestSurveyMP_Ctlr( LO_StandardController );
        System.assertEquals( 2, LO_SurveyController.GV_IncentiveType );
        System.assertEquals( 'ContactName', LO_SurveyController.GV_ContactName );

        LO_SurveyController.GV_Sensitivity1 = 'Improved';
        LO_SurveyController.GV_Reproducibility1 = 'Improved';
        LO_SurveyController.GV_Specificity1 = 'Improved';
        LO_SurveyController.GV_EaseOfUse1 = 'Improved';
        LO_SurveyController.GV_Overall1 = 'Improved';
        LO_SurveyController.GV_EvaluationComment1 = 'Comment';
        LO_SurveyController.GV_Recommendation1 = 'Yes';
        LO_SurveyController.GV_ApproveTestimonial1 = true;

        LO_SurveyController.GV_Sensitivity2 = 'Improved';
        LO_SurveyController.GV_Reproducibility2 = 'Improved';
        LO_SurveyController.GV_Specificity2 = 'Improved';
        LO_SurveyController.GV_EaseOfUse2 = 'Improved';
        LO_SurveyController.GV_Overall2 = 'Improved';
        LO_SurveyController.GV_EvaluationComment2 = 'Comment';
        LO_SurveyController.GV_Recommendation2 = 'Yes';
        LO_SurveyController.GV_ApproveTestimonial2 = true;

		LO_SurveyController.GV_Sensitivity3 = 'Improved';
        LO_SurveyController.GV_Reproducibility3 = 'Improved';
        LO_SurveyController.GV_Specificity3 = 'Improved';
        LO_SurveyController.GV_EaseOfUse3 = 'Improved';
        LO_SurveyController.GV_Overall3 = 'Improved';
        LO_SurveyController.GV_EvaluationComment3 = 'Comment';
        LO_SurveyController.GV_Recommendation3 = 'Yes';
        LO_SurveyController.GV_ApproveTestimonial3 = true;

		LO_SurveyController.GV_Sensitivity4 = 'Improved';
        LO_SurveyController.GV_Reproducibility4 = 'Improved';
        LO_SurveyController.GV_Specificity4 = 'Improved';
        LO_SurveyController.GV_EaseOfUse4 = 'Improved';
        LO_SurveyController.GV_Overall4 = 'Improved';
        LO_SurveyController.GV_EvaluationComment4 = 'Comment';
        LO_SurveyController.GV_Recommendation4 = 'Yes';
        LO_SurveyController.GV_ApproveTestimonial4 = true;

        LO_SurveyController.GV_CurrentCustomer = 'Yes';
        LO_SurveyController.GV_Incentive = '2for3';

        LO_SurveyController.saveSurvey();

        Test.stopTest();

        LO_Opportunity = [
                SELECT Sensitivity__c,
                        Reproducibility__c,
                        Specificity__c,
                        Ease_of_use__c,
                        Overall__c,
                        Evaluation_Comment__c,
                        Recommendation__c,
                        Current_Customer__c,
                        Gift_Choice__c
                FROM Opportunity
        ];

        System.assertEquals( LO_SurveyController.GV_CurrentCustomer, LO_Opportunity.Current_Customer__c );
        System.assertEquals( LO_SurveyController.GV_Incentive, LO_Opportunity.Gift_Choice__c );

		/*
        OpportunityLineItem LO_OpportunityLines = [
                SELECT Sensitivity__c,
                        Reproducibility__c,
                        Specificity__c,
                        Ease_of_use__c,
                        Overall__c,
                        Evaluation_Comment__c,
                        Recommendation__c,
                        Current_Customer__c,
                        Gift_Choice__c
                FROM OpportunityLineItem
        ];
		
        System.assertEquals( LO_SurveyController.GV_Sensitivity, LO_Opportunity.Sensitivity__c );
        System.assertEquals( LO_SurveyController.GV_Reproducibility, LO_Opportunity.Reproducibility__c );
        System.assertEquals( LO_SurveyController.GV_Specificity, LO_Opportunity.Specificity__c );
        System.assertEquals( LO_SurveyController.GV_EaseOfUse, LO_Opportunity.Ease_of_use__c );
        System.assertEquals( LO_SurveyController.GV_Overall, LO_Opportunity.Overall__c );
        System.assertEquals( LO_SurveyController.GV_EvaluationComment, LO_Opportunity.Evaluation_Comment__c );
        System.assertEquals( LO_SurveyController.GV_Recommendation, LO_Opportunity.Recommendation__c );
		*/

    }

    static Opportunity createTestData() {
        Account LO_Account = ( Account ) HW_TestTools.createRecord( 'Account' );
        insert( LO_Account );

        Contact LO_Contact = ( Contact ) HW_TestTools.createRecord( 'Contact' );
        LO_Contact.AccountId = LO_Account.Id;
        LO_Contact.LastName = 'ContactName';
        LO_Contact.Email = 'test@hundw.com';
        insert( LO_Contact );

        Opportunity LO_Opportunity = ( Opportunity ) HW_TestTools.createRecord( 'Opportunity' );
        LO_Opportunity.AccountId = LO_Account.Id;
        LO_Opportunity.Sample_Request_Type__c = 'Regular Sample Request';
        LO_Opportunity.Lead_Creator__c = UserInfo.getUserId();
        insert( LO_Opportunity );

        OpportunityContactRole LO_OpportunityContactRole = ( OpportunityContactRole ) HW_TestTools.createRecord( 'OpportunityContactRole' );
        LO_OpportunityContactRole.OpportunityId = LO_Opportunity.Id;
        LO_OpportunityContactRole.ContactId = LO_Contact.Id;
        insert( LO_OpportunityContactRole );

		//Adding 4 product Descriptions, products, Pricebook Entries, and Opportunity Lines to test the new multi products code
		List<Product_Description__c> lPDesc = new List<Product_Description__c>();
		for(Integer i = 0 ; i < 4 ; i++){
			Product_Description__c pd = new Product_Description__c();
			pd.Name = 'TestProductDesc' + String.valueOf(i);
			lPDesc.add(pd);
		}
		insert lPDesc;
		List<Product2> lProducts = new List<Product2>();
		for(Integer i = 0 ; i < 4 ; i++){
			Product2 p = new Product2();
			p.Name = 'TestProduct' + String.valueOf(i);
			p.ProductCode__c = 'TestProduct' + String.valueOf(i);
			p.Catalog_QUANTA__c = 'TestProduct' + String.valueOf(i);
			p.Product_Sales_Type__c = 'Regular Product';
			p.Product_Description__c = lPDesc[i].Id;
			lProducts.add(p);
		}
		insert lProducts;
		//Pricebook2 stdPB = [SELECT Id, IsActive FROM PriceBook2 WHERE IsStandard=True];
		List<PricebookEntry> lPBE = new List<PricebookEntry>();
		for(Integer i = 0 ; i < 4 ; i++){
			PricebookEntry pbe = new PricebookEntry();
			pbe.Pricebook2Id = Test.getStandardPricebookId();
			pbe.Product2Id = lProducts[i].Id;
			pbe.IsActive = true;
			pbe.UnitPrice = i * 100.0;
			lPBE.add(pbe);
		}
		insert lPBE;
		List<OpportunityLineItem> lOLI = new List<OpportunityLineItem>();
		for(Integer i = 0 ; i < 4 ; i++){
			OpportunityLineItem oli = new OpportunityLineItem();
			oli.OpportunityId = LO_Opportunity.Id;
			oli.PricebookEntryId = lPBE[i].Id;
			oli.Quantity = 1;
			oli.UnitPrice = lPBE[i].UnitPrice;
			lOLI.add(oli);
		}
		insert lOLI;



        return LO_Opportunity;
    }

}