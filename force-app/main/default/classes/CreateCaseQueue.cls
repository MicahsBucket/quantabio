public class CreateCaseQueue implements Queueable{
    public Lead le;
    public CreateCaseQueue(Lead ld) {
        this.le = ld;
    }
    public void execute(QueueableContext context) {
        Case newCase = new Case();
        newCase.Case_Type__c = 'Technical Support';
        newCase.Commercial_Lead__c = 'NO';
        newCase.Country__c = le.Country__c;
        newCase.Description = le.Subject__c;
        newCase.First_Name__c = le.FirstName;
        newCase.Last_Name__c = le.LastName;
        newCase.Origin = 'Web Chat';
        newCase.Priority = 'TBD';
        newCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Quantabio Technical Support').getRecordTypeId();
        newCase.Status = 'New';
        newCase.Subject = le.Subject__c;
        newCase.Email_Web__c = le.Email;
        newCase.SuppliedEmail = le.Email;
        newCase.SuppliedCompany = le.Company;
        newCase.Company_Web__c = le.Company;
        system.debug(newCase);
        insert newCase;

        this.le.Case__c = newCase.Id;
        update this.le;


    }

}
