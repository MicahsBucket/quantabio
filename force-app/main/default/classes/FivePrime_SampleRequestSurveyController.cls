/**
 * Created by oliverpreuschl on 25.05.16.
 */

public without sharing class FivePrime_SampleRequestSurveyController {

    public Opportunity GO_Opportunity{ get; set; }

    public String GV_Sensitivity{ get; set; }
    public String GV_Reproducibility{ get; set; }
    public String GV_Specificity{ get; set; }
    public String GV_EaseOfUse{ get; set; }
    public String GV_Overall{ get; set; }
    public String GV_EvaluationComment{ get; set; }
    public String GV_Recommendation{ get; set; }
    public String GV_CurrentCustomer{ get; set; }
    public String GV_Incentive{ get; set; }

    public Integer GV_IncentiveType{
        get{
            if( GV_IncentiveType == null ) {
                String LV_IncentiveType = ApexPages.currentPage().getParameters().get( 't' );
                if( String.isNotBlank( LV_IncentiveType ) ) {
                    GV_IncentiveType = Integer.valueOf( LV_IncentiveType );
                }
            }
            return GV_IncentiveType;
        }
        set;
    }

    public String GV_ContactName{
        get{
            if( GV_ContactName == null ) {
                try{
                    OpportunityContactRole LO_OpportunityContactRole = [ SELECT Contact.Name FROM OpportunityContactRole WHERE ( OpportunityId = :GO_Opportunity.Id ) ];
                    GV_ContactName = LO_OpportunityContactRole.Contact.Name;
                }catch( Exception LO_Exception ){
                    GV_ContactName = '';
                }
            }
            return GV_ContactName;
        }
        set;
    }

    public FivePrime_SampleRequestSurveyController( ApexPages.StandardController PO_Controller ){
        Id LV_OpportunityId = PO_Controller.getId();
        try{
            LV_OpportunityId = PO_Controller.getId();
        }catch( Exception LO_Exception ){
        }
        if( LV_OpportunityId != null ){
            GO_Opportunity = [ SELECT Id, Sensitivity__c FROM Opportunity WHERE ( Id = :LV_OpportunityId ) ];
        }
    }

    public PageReference saveSurvey(){
        GO_Opportunity.Questionnaire_Done__c = Date.today();
        GO_Opportunity.Sensitivity__c = GV_Sensitivity;
        GO_Opportunity.Reproducibility__c = GV_Reproducibility;
        GO_Opportunity.Specificity__c = GV_Specificity;
        GO_Opportunity.Ease_of_use__c = GV_EaseOfUse;
        GO_Opportunity.Overall__c = GV_Overall;
        GO_Opportunity.Evaluation_Comment__c = GV_EvaluationComment;
        GO_Opportunity.Recommendation__c = GV_Recommendation;
        GO_Opportunity.Current_Customer__c = GV_CurrentCustomer;
        GO_Opportunity.Gift_Choice__c = GV_Incentive;
        update( GO_Opportunity );

        PageReference LO_ThankYouPage = Page.FivePrime_SurveyThankYou;
        LO_ThankYouPage.setRedirect( true );

        return LO_ThankYouPage;
    }

}