@isTest
public with sharing class relatedTechSupportLeadToCaseTEST {
    @isTest
    public static void createLead() {
        Lead l = new Lead();
        l.Status = 'New';
        l.FirstName = 'Test';
        l.LastName = 'Tester';
        l.Company = 'Test Company';
        l.Country__c = 'Canada';
        l.CountryCode = 'CA';
        l.Country = 'Canada';
        l.LeadSource = 'Other';
        l.Lead_Type__c = 'Technical Service';
        l.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Lead').getRecordTypeId();

        insert l;
        List<Lead> leadList = new List<Lead>();
        leadList.add(l);

        relatedTechSupportLeadToCase.createCase(leadList);
    }
}
