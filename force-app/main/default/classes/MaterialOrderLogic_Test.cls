/**
*   {Purpose}  –  test class for the MaterialOrderLogic class
*
*   {Function}  –
*
*   {Support}   - For assistance with this code, please contact support@demandchainsystems.com
*                 www.demandchainsystems.com
*                 (952) 345-4533
*/
/**
*   CHANGE  HISTORY
*   =============================================================================
*   Date      Name             Description
*   20181128  EBG DCS          Created
*   =============================================================================*/
@isTest
private class MaterialOrderLogic_Test {

    @isTest static void testOrderCreate() {
		List<Marketing_Material__c> lstMaterials_Beverly = populateTestData_DCS.createMarketingMaterial(3);
        Marketing_Kit__c oKit_Beverly = populateTestData_DCS.createMarketingKit('Beverly', lstMaterials_Beverly);

        List<Marketing_Material__c> lstMaterials_Hilden = populateTestData_DCS.createMarketingMaterial(3);
        Marketing_Kit__c oKit_Hilden = populateTestData_DCS.createMarketingKit('Hilden', lstMaterials_Hilden);
        
        Test.startTest();
        
        Office_Order__c oOrder_Beverly = populateTestData_DCS.createMaterialOrder(oKit_Beverly);
        //test removal of kit from order to delete line items
        oOrder_Beverly.Marketing_Kit__c = null;
        update oOrder_Beverly;
            
        Office_Order__c oOrder_Hilden = populateTestData_DCS.createMaterialOrder(oKit_Hilden);
        //test the changing of a quantity of an existing line item
        for (Materials__c oMaterial : [Select Id
                                       		, Office_Order__c
                                       		, Quantity__c
                                       		, Marketing_Material_Order__c
                                       		, Marketing_Material__c 
                                       From Materials__c 
                                       Where Office_Order__c = :oOrder_Hilden.Id
                                       LIMIT 1]) {
			oMaterial.Quantity__c = oMaterial.Quantity__c + 1;
			update oMaterial;
			oMaterial.Quantity__c = oMaterial.Quantity__c - 1;
			update oMaterial;
		}

		Test.stopTest();
    }
}