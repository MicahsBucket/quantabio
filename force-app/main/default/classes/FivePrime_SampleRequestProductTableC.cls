/**
* @author           oliverpreuschl
*                   H&W Consult GmbH
*                   Bahnhofstr. 3
*                   21244 Buchholz i.d.N.
*                   http://www.hundw.com
*
* @description      
*
* @date             24.02.17 
*
* Timeline:
* Name              Date                Version        Description
* oliverpreuschl           24.02.17             *1.0*          Created the class
*/

public with sharing class FivePrime_SampleRequestProductTableC {

    public Id GV_OpportunityId{
        get;
        set;
    }

    private Set< Id > GS_ProductIds{
        get{
            if( GS_ProductIds == null ){
                GS_ProductIds = new Set< Id >();
                for( OpportunityLineItem LO_OpportunityLineItem: [ SELECT Id, Product2Id FROM OpportunityLineItem WHERE ( OpportunityId = :GV_OpportunityId ) ] ){
                    GS_ProductIds.add( LO_OpportunityLineItem.Product2Id );
                }
            }
            return GS_ProductIds;
        }
        set;
    }

    private List< String > GL_CatalogNumberPrefixes{
        get{
            if( GL_CatalogNumberPrefixes == null ){
                GL_CatalogNumberPrefixes = new List< String >();
                for( Product2 LO_Product: [ SELECT Id, Catalog_QUANTA__c FROM Product2 WHERE ( Id = :GS_ProductIds ) ] ){
                    if( String.isNotBlank( LO_Product.Catalog_QUANTA__c ) ) {
                        List< String > LL_CatalogNumber = LO_Product.Catalog_QUANTA__c.split( '-' );
                        GL_CatalogNumberPrefixes.add( LL_CatalogNumber.get( 0 ) + '%' );
                    }
                }
            }
            return GL_CatalogNumberPrefixes;
        }
        set;
    }

    public List< Product_Description_Junction__c > GL_RelatedProductDescriptionJunctions{
        get{
            if( GL_RelatedProductDescriptionJunctions == null ){
                GL_RelatedProductDescriptionJunctions = new List< Product_Description_Junction__c >();
                for( Product_Description_Junction__c LO_ProductDescriptionJunction: [ SELECT Id, QB_Catalog_Number__c, Product__c, Product__r.Name, Product__r.isActive  FROM Product_Description_Junction__c WHERE ( QB_Catalog_Number__c LIKE :GL_CatalogNumberPrefixes AND Product__r.isActive = true ) ] ){
                    GL_RelatedProductDescriptionJunctions.add( LO_ProductDescriptionJunction );
                }
            }
            return GL_RelatedProductDescriptionJunctions;
        }
        set;
    }

}