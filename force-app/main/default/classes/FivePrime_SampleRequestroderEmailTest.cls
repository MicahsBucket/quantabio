/**
 * Created by oliverpreuschl on 18.03.16.
 */

@IsTest
private with sharing class FivePrime_SampleRequestroderEmailTest {

    static testMethod void testEmailSending(){
        createTestData();

        Test.startTest();

        Opportunity LO_Opportunity = [ SELECT Id, SR_Order_Email_Trigger__c FROM Opportunity ];
        LO_Opportunity.SR_Order_Email_Trigger__c = 'Confirmation';
        update( LO_Opportunity );

        Test.stopTest();

        LO_Opportunity = [ SELECT Id, SR_Order_Email_Trigger__c, SR_Order_Confirmation_Email_Date__c FROM Opportunity ];
        //System.assertEquals( Date.today(), LO_Opportunity.SR_Order_Confirmation_Email_Date__c );
    }

    static void createTestData(){
        Account LO_Account = ( Account ) HW_TestTools.createRecord( 'Account' );
        insert( LO_Account );

        Contact LO_Contact = ( Contact ) HW_TestTools.createRecord( 'Contact' );
        LO_Contact.AccountId = LO_Account.Id;
        LO_Contact.Email = 'test@hundw.com';
        insert( LO_Contact );

        Opportunity LO_Opportunity = ( Opportunity ) HW_TestTools.createRecord( 'Opportunity' );
        LO_Opportunity.AccountId = LO_Account.Id;
        LO_Opportunity.Sample_Request_Type__c = 'Regular Sample Request';
        LO_Opportunity.Lead_Creator__c = UserInfo.getUserId();
        LO_Opportunity.Generalist_Email__c = 'test@invalid.com';
        LO_Opportunity.Requestor_Email_LSS__c = 'test@invalid.com';
        insert( LO_Opportunity );

        OpportunityContactRole LO_OpportunityContactRole = ( OpportunityContactRole ) HW_TestTools.createRecord( 'OpportunityContactRole' );
        LO_OpportunityContactRole.OpportunityId = LO_Opportunity.Id;
        LO_OpportunityContactRole.ContactId = LO_Contact.Id;
        LO_OpportunityContactRole.ContactId = LO_Contact.Id;
        insert( LO_OpportunityContactRole );
    }

}