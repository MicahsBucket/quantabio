public class FivePrime_SampleRequestSurveyMP_Ctlr  {
/**
 * This class derrived from "FivePrime_SampleRequestSurveyController" Created by oliverpreuschl on 25.05.16.
 * Modified by Brian Tremblay of Demand Chain
 *      Modified on 7/24/2018
 *      Modified to support Multiple Opportunity Products
 */

    public Opportunity GO_Opportunity{ get; set; }
    public List<OpportunityLineItem> GO_OpportunityProducts{ get; set; }

    public String GV_CurrentCustomer{ get; set; }
    public String GV_Incentive{ get; set; }
    //Product Set 1
    public String GV_Tested1{ get; set; }
    public String GV_Interested1{ get; set; }
    public String GV_Sensitivity1{ get; set; }
    public String GV_Reproducibility1{ get; set; }
    public String GV_Specificity1{ get; set; }
    public String GV_EaseOfUse1{ get; set; }
    public String GV_Overall1{ get; set; }
    public String GV_EvaluationComment1{ get; set; }
    public String GV_Recommendation1{ get; set; }
    public Boolean GV_ApproveTestimonial1{ get; set; }
    //public Boolean GV_NoTestNoInterest1{ get; set; }
    //public Boolean GV_NoTestYesInterest1{ get; set; }
    //Product Set 2
    public String GV_Tested2{ get; set; }
    public String GV_Interested2{ get; set; }
    public String GV_Sensitivity2{ get; set; }
    public String GV_Reproducibility2{ get; set; }
    public String GV_Specificity2{ get; set; }
    public String GV_EaseOfUse2{ get; set; }
    public String GV_Overall2{ get; set; }
    public String GV_EvaluationComment2{ get; set; }
    public String GV_Recommendation2{ get; set; }
    public Boolean GV_ApproveTestimonial2{ get; set; }
    //public Boolean GV_NoTestNoInterest2{ get; set; }
    //public Boolean GV_NoTestYesInterest2{ get; set; }
    //Product Set 3
    public String GV_Tested3{ get; set; }
    public String GV_Interested3{ get; set; }
    public String GV_Sensitivity3{ get; set; }
    public String GV_Reproducibility3{ get; set; }
    public String GV_Specificity3{ get; set; }
    public String GV_EaseOfUse3{ get; set; }
    public String GV_Overall3{ get; set; }
    public String GV_EvaluationComment3{ get; set; }
    public String GV_Recommendation3{ get; set; }
    public Boolean GV_ApproveTestimonial3{ get; set; }
    //public Boolean GV_NoTestNoInterest3{ get; set; }
    //public Boolean GV_NoTestYesInterest3{ get; set; }
    //Product Set 4
    public String GV_Tested4{ get; set; }
    public String GV_Interested4{ get; set; }
    public String GV_Sensitivity4{ get; set; }
    public String GV_Reproducibility4{ get; set; }
    public String GV_Specificity4{ get; set; }
    public String GV_EaseOfUse4{ get; set; }
    public String GV_Overall4{ get; set; }
    public String GV_EvaluationComment4{ get; set; }
    public String GV_Recommendation4{ get; set; }
    public Boolean GV_ApproveTestimonial4{ get; set; }
    //public Boolean GV_NoTestNoInterest4{ get; set; }
    //public Boolean GV_NoTestYesInterest4{ get; set; }

    public string ERR_TestedYes = ', you indicated it was tested - please complete the Sensitivity, Ease-of-use and Overall Performance rating.';
    public string ERR_TestedNo = ', you indicated it was not tested - please let us know if you are still interested.';
    public string ERR_TestedNull = ', please indicate if it was tested or not.';

    public Integer GV_IncentiveType{
        get{
            if( GV_IncentiveType == null ) {
                String LV_IncentiveType = ApexPages.currentPage().getParameters().get( 't' );
                if( String.isNotBlank( LV_IncentiveType ) ) {
                    GV_IncentiveType = Integer.valueOf( LV_IncentiveType );
                }
            }
            return GV_IncentiveType;
        }
        set;
    }

    public String GV_ContactName{
        get{
            if( GV_ContactName == null ) {
                try{
                    OpportunityContactRole LO_OpportunityContactRole = [ SELECT Contact.Name FROM OpportunityContactRole WHERE ( OpportunityId = :GO_Opportunity.Id ) ];
                    GV_ContactName = LO_OpportunityContactRole.Contact.Name;
                }catch( Exception LO_Exception ){
                    GV_ContactName = '';
                }
            }
            return GV_ContactName;
        }
        set;
    }

    public String GV_ContactFirstName{
        get{
            if( GV_ContactFirstName == null ) {
                try{
                    OpportunityContactRole LO_OpportunityContactRole = [ SELECT Contact.FirstName FROM OpportunityContactRole WHERE ( OpportunityId = :GO_Opportunity.Id ) ];
                    GV_ContactFirstName = LO_OpportunityContactRole.Contact.FirstName;
                }catch( Exception LO_Exception ){
                    GV_ContactFirstName = '';
                }
            }
            return GV_ContactFirstName;
        }
        set;
    }
    //public FivePrime_SampleRequestSurveyMP_Ctlr(  ){ }
    public FivePrime_SampleRequestSurveyMP_Ctlr( ApexPages.StandardController PO_Controller ){
        Id LV_OpportunityId = PO_Controller.getId();
        try{
            LV_OpportunityId = PO_Controller.getId();
        }catch( Exception LO_Exception ){
        }
        if( LV_OpportunityId != null ){
            GO_Opportunity = [ SELECT Id, Sensitivity__c FROM Opportunity WHERE ( Id = :LV_OpportunityId ) ];
            GO_OpportunityProducts = [SELECT Id,ProductCode,Product2.Name,Product2Id FROM OpportunityLineItem WHERE OpportunityId = :LV_OpportunityId];
        }
        GV_ApproveTestimonial1 = true;
        GV_ApproveTestimonial2 = true;
        GV_ApproveTestimonial3 = true;
        GV_ApproveTestimonial4 = true;
    }

    private Boolean boolMe(String s)
    {
        Boolean returnBool = false;
        if(s == 'true')
        {
            returnBool = true;
        }
        return returnBool;
    }

    public PageReference saveSurvey() {

        System.debug('DC: ' + Apexpages.currentPage().getParameters().get('GV_ApproveTestimonial1'));  

        GV_Tested1 = Apexpages.currentPage().getParameters().get('GV_Tested1');
        GV_Interested1 = Apexpages.currentPage().getParameters().get('GV_Interested1');
        GV_Sensitivity1 = Apexpages.currentPage().getParameters().get('GV_Sensitivity1');
        GV_EaseOfUse1 = Apexpages.currentPage().getParameters().get('GV_EaseOfUse1');
        GV_Overall1 = Apexpages.currentPage().getParameters().get('GV_Overall1');
        GV_EvaluationComment1 = Apexpages.currentPage().getParameters().get('GV_EvaluationComment1');
        GV_ApproveTestimonial1 = boolMe(Apexpages.currentPage().getParameters().get('GV_ApproveTestimonial1'));

        GV_Tested2 = Apexpages.currentPage().getParameters().get('GV_Tested2');
        GV_Interested2 = Apexpages.currentPage().getParameters().get('GV_Interested2');
        GV_Sensitivity2 = Apexpages.currentPage().getParameters().get('GV_Sensitivity2');
        GV_EaseOfUse2 = Apexpages.currentPage().getParameters().get('GV_EaseOfUse2');
        GV_Overall2 = Apexpages.currentPage().getParameters().get('GV_Overall2');
        GV_EvaluationComment2 = Apexpages.currentPage().getParameters().get('GV_EvaluationComment2');
        GV_ApproveTestimonial2 = boolMe(Apexpages.currentPage().getParameters().get('GV_ApproveTestimonial2'));

        GV_Tested3 = Apexpages.currentPage().getParameters().get('GV_Tested3');
        GV_Interested3 = Apexpages.currentPage().getParameters().get('GV_Interested3');
        GV_Sensitivity3 = Apexpages.currentPage().getParameters().get('GV_Sensitivity3');
        GV_EaseOfUse3 = Apexpages.currentPage().getParameters().get('GV_EaseOfUse3');
        GV_Overall3 = Apexpages.currentPage().getParameters().get('GV_Overall3');
        GV_EvaluationComment3 = Apexpages.currentPage().getParameters().get('GV_EvaluationComment3');
        GV_ApproveTestimonial3 = boolMe(Apexpages.currentPage().getParameters().get('GV_ApproveTestimonial3'));

        GV_Tested4 = Apexpages.currentPage().getParameters().get('GV_Tested4');
        GV_Interested4 = Apexpages.currentPage().getParameters().get('GV_Interested4');
        GV_Sensitivity4 = Apexpages.currentPage().getParameters().get('GV_Sensitivity4');
        GV_EaseOfUse4 = Apexpages.currentPage().getParameters().get('GV_EaseOfUse4');
        GV_Overall4 = Apexpages.currentPage().getParameters().get('GV_Overall4');
        GV_EvaluationComment4 = Apexpages.currentPage().getParameters().get('GV_EvaluationComment4');
        GV_ApproveTestimonial4 = boolMe(Apexpages.currentPage().getParameters().get('GV_ApproveTestimonial4'));

        GV_CurrentCustomer = Apexpages.currentPage().getParameters().get('GV_CurrentCustomer');
        GV_Incentive = Apexpages.currentPage().getParameters().get('GV_Incentive');

        System.debug('DC: ' + GO_OpportunityProducts.size());
        if(GO_OpportunityProducts.size() > 0){

            //Product Set 1
            GO_OpportunityProducts[0].Tested__c = GV_Tested1;
            GO_OpportunityProducts[0].Interested__c = GV_Interested1;
            GO_OpportunityProducts[0].Sensitivity__c = GV_Sensitivity1;
            GO_OpportunityProducts[0].Reproducibility__c = GV_Reproducibility1;
            GO_OpportunityProducts[0].Specificity__c = GV_Specificity1;
            GO_OpportunityProducts[0].Ease_of_use__c = GV_EaseOfUse1;
            GO_OpportunityProducts[0].Overall__c = GV_Overall1;
            GO_OpportunityProducts[0].Evaluation_Comment__c = GV_EvaluationComment1;
            GO_OpportunityProducts[0].Recommendation__c = GV_Recommendation1;
            GO_OpportunityProducts[0].Public_Testimonial_Approved__c = GV_ApproveTestimonial1;
            //GO_OpportunityProducts[0].Not_Tested_No_Interest__c = GV_NoTestNoInterest1;
            //GO_OpportunityProducts[0].Not_Tested_With_Interest__c = GV_NoTestYesInterest1;
        }
        if(GO_OpportunityProducts.size() > 1){
            //Product Set 2
            GO_OpportunityProducts[1].Tested__c = GV_Tested2;
            GO_OpportunityProducts[1].Interested__c = GV_Interested2;
            GO_OpportunityProducts[1].Sensitivity__c = GV_Sensitivity2;
            GO_OpportunityProducts[1].Reproducibility__c = GV_Reproducibility2;
            GO_OpportunityProducts[1].Specificity__c = GV_Specificity2;
            GO_OpportunityProducts[1].Ease_of_use__c = GV_EaseOfUse2;
            GO_OpportunityProducts[1].Overall__c = GV_Overall2;
            GO_OpportunityProducts[1].Evaluation_Comment__c = GV_EvaluationComment2;
            GO_OpportunityProducts[1].Recommendation__c = GV_Recommendation2;
            GO_OpportunityProducts[1].Public_Testimonial_Approved__c = GV_ApproveTestimonial2;
            //GO_OpportunityProducts[1].Not_Tested_No_Interest__c = GV_NoTestNoInterest2;
            //GO_OpportunityProducts[1].Not_Tested_With_Interest__c = GV_NoTestYesInterest2;
        }
        if(GO_OpportunityProducts.size() > 2){
            //Product Set 3
            GO_OpportunityProducts[2].Tested__c = GV_Tested3;
            GO_OpportunityProducts[2].Interested__c = GV_Interested3;
            GO_OpportunityProducts[2].Sensitivity__c = GV_Sensitivity3;
            GO_OpportunityProducts[2].Reproducibility__c = GV_Reproducibility3;
            GO_OpportunityProducts[2].Specificity__c = GV_Specificity3;
            GO_OpportunityProducts[2].Ease_of_use__c = GV_EaseOfUse3;
            GO_OpportunityProducts[2].Overall__c = GV_Overall3;
            GO_OpportunityProducts[2].Evaluation_Comment__c = GV_EvaluationComment3;
            GO_OpportunityProducts[2].Recommendation__c = GV_Recommendation3;
            GO_OpportunityProducts[2].Public_Testimonial_Approved__c = GV_ApproveTestimonial3;
            //GO_OpportunityProducts[2].Not_Tested_No_Interest__c = GV_NoTestNoInterest3;
            //GO_OpportunityProducts[2].Not_Tested_With_Interest__c = GV_NoTestYesInterest3;
        }
        if(GO_OpportunityProducts.size() > 3){
            //Product Set 4
            GO_OpportunityProducts[3].Tested__c = GV_Tested4;
            GO_OpportunityProducts[3].Interested__c = GV_Interested4;
            GO_OpportunityProducts[3].Sensitivity__c = GV_Sensitivity4;
            GO_OpportunityProducts[3].Reproducibility__c = GV_Reproducibility4;
            GO_OpportunityProducts[3].Specificity__c = GV_Specificity4;
            GO_OpportunityProducts[3].Ease_of_use__c = GV_EaseOfUse4;
            GO_OpportunityProducts[3].Overall__c = GV_Overall4;
            GO_OpportunityProducts[3].Evaluation_Comment__c = GV_EvaluationComment4;
            GO_OpportunityProducts[3].Recommendation__c = GV_Recommendation4;
            GO_OpportunityProducts[3].Public_Testimonial_Approved__c = GV_ApproveTestimonial4;
            //GO_OpportunityProducts[3].Not_Tested_No_Interest__c = GV_NoTestNoInterest4;
            //GO_OpportunityProducts[3].Not_Tested_With_Interest__c = GV_NoTestYesInterest4;
        }

        System.debug('DC: GO_OpportunityProducts: ' + GO_OpportunityProducts);
        update GO_OpportunityProducts;
        
        GO_Opportunity.Questionnaire_Done__c = Date.today();
        GO_Opportunity.Current_Customer__c = GV_CurrentCustomer;
        GO_Opportunity.Gift_Choice__c = GV_Incentive;
        System.debug('Updating opportunity');
        update( GO_Opportunity );
        

        //create testimonials
        Id ContactId = [ SELECT Contact.Id FROM OpportunityContactRole WHERE ( OpportunityId = :GO_Opportunity.Id ) ].Contact.Id;
        System.debug('DC: contactid: ' + ContactId);
        Map<Id,OpportunityLineItem> mProductID_ProductDescId = new Map<Id,OpportunityLineItem>();
        for(OpportunityLineItem oli : GO_OpportunityProducts){
            if(oli.Evaluation_Comment__c != null && oli.Evaluation_Comment__c.length() > 2  && oli.Public_Testimonial_Approved__c == true){
                mProductID_ProductDescId.put(oli.Product2Id,oli);
            }
        }
        System.debug('DC: mProductID_ProductDescId: ' + mProductID_ProductDescId);
        List<Product2> lProducts = [SELECT Id,Product_Description__c FROM Product2 WHERE Id IN :mProductID_ProductDescId.keySet()];
        System.debug('DC: lProducts: ' + lProducts);
        List<Testimonial__c>    newTestimonials = new List<Testimonial__c>();
        for(Product2 p : lProducts){
            Testimonial__c t = new Testimonial__c();
            t.To_publish_on_web__c = 'Do not publish';
            t.Contact__c = ContactId;
            t.Customer_Name__c = GV_ContactName;
            t.Customer_Testimonial__c = mProductID_ProductDescId.get(p.Id).Evaluation_Comment__c;
            t.Product_Description__c = p.Product_Description__c;
            newTestimonials.add(t);
        }
        System.debug('DC: newTestimonials: ' + newTestimonials);
        insert newTestimonials;

        //redirect user to thankyou
        PageReference LO_ThankYouPage = Page.FivePrime_SurveyThankYou;
        LO_ThankYouPage.setRedirect( true );

        return LO_ThankYouPage;
    }


}