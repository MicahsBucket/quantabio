/**
* @author           oliverpreuschl
*                   H&W Consult GmbH
*                   Bahnhofstr. 3
*                   21244 Buchholz i.d.N.
*                   http://www.hundw.com
*
* @description      
*
* @date             15.08.17 
*
* Timeline:
* Name              Date                Version        Description
* oliverpreuschl           15.08.17             *1.0*          Created the class
*/

public with sharing class FivePrime_SampleRequestConfirmationC {

    public Lead GO_Lead{ get; set; }
    public String GV_CustomerComment{
        get{
            if( GV_CustomerComment == null ){
                GV_CustomerComment = '';
            }
            return GV_CustomerComment;
        }
        set;
    }

    public String GV_Response {
        get {
            if( GV_Response == null ) {
                if( ApexPages.currentPage().getParameters().containsKey( 'response' ) ) {
                    GV_Response = ApexPages.currentPage().getParameters().get( 'response' );
                }
            }
            return GV_Response;
        }
        set;
    }

    private Set< String > GS_ValidStatuses = new Set< String >{
            'Confirmed',
            'Declined',
            'Call'
    };

    public Boolean GV_Success_1 { get; set; }
    public Boolean GV_Success_2 { get; set; }
    public Boolean GV_Error{
        get{
            if( ( GV_Success_1 != null && !GV_Success_1 ) || ( GV_Success_2 != null && !GV_Success_2 ) ){
                GV_Error = true;
            }else{
                GV_Error = false;
            }
            return GV_Error;
        }
        set;
    }
    public Boolean GV_ShowComment{
        get{
            if( !GV_Error && ( GV_Success_2 == null ) ){
                GV_ShowComment = true;
            }else{
                GV_ShowComment = false;
            }
            return GV_ShowComment;
        }
        set;
    }

    public FivePrime_SampleRequestConfirmationC( ApexPages.StandardController PO_Controller ) {
        if( !Test.isRunningTest() ) {
            PO_Controller.addFields(
                    new List< String >{
                            'Name',
                            'Customer_comment_on_sample_order__c'
                    }
            );
        }
        GO_Lead = ( Lead ) PO_Controller.getRecord();
    }

    public PageReference saveResponse() {
        if( String.isNotBlank( GV_Response ) && GS_ValidStatuses.contains( GV_Response ) ) {
            try {
                GO_Lead.Sample_Request_Status__c = GV_Response;
                update( GO_Lead );
                GV_Success_1 = true;
            } catch( Exception LO_Exception ) {
                System.debug( 'Exception : ' + LO_Exception.getMessage() + ', ' + LO_Exception.getLineNumber() );
                GV_Success_1 = false;
            }
        }else{
            GV_Success_1 = false;
        }
        return null;
    }

    public PageReference saveComment(){
        try {
            Lead LO_Lead = new Lead(
                    Id                                      = GO_Lead.Id,
                    Customer_comment_on_sample_order__c     = GV_CustomerComment.left( 255 )
            );
            update( LO_Lead );
            GV_Success_2 = true;
        } catch( Exception LO_Exception ) {
            System.debug( 'Exception: ' + LO_Exception.getMessage() + ', ' + LO_Exception.getLineNumber() );
            GV_Success_2 = false;
        }
        return null;
    }

}