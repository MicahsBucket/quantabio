@isTest
public with sharing class AgentWorkTriggerTest {
    @isTest(seeAllData=true)
    public static void AgentWorkTest() {
        ServiceChannel serviceChannel = [SELECT Id FROM ServiceChannel WHERE Masterlabel = 'Chat'];

        User user1 = new User();
        user1.id = UserInfo.getUserId();
        System.runAs(user1){    
            AgentWork aw = new AgentWork();
            aw.UserId = user1.Id;
            aw.ServiceChannelId = serviceChannel.Id;
            LiveChatTranscript t = [SELECT Id, Name, Status FROM LiveChatTranscript WHERE Status != 'Missed' LIMIT 1];
            aw.WorkItemId = t.Id;
            insert aw;
        }
    }
}
