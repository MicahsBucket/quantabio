/**
*   {Purpose}  –  test class that loads data for other tests
*
*   {Function}  –  
*                  
*   {Support}   - For assistance with this code, please contact support@demandchain.com              
*                 www.demandchain.com
*                 (952) 345-4533
*/
/**
*   CHANGE  HISTORY
*   =============================================================================
*   Date      Name             Description
*   20190118  CAM DCS          Created
*   20190506  CAM DCS          Added Isactive to Product2 creation
*   20190627  CAM DCS          Added ProductCode to Product2 creation
*
*   =============================================================================*/
@isTest
public class PopulateTestData_DC {
    ////////////////////////////////////////////////////////////////////////////////
    //create test Sample Request Lead
    ////////////////////////////////////////////////////////////////////////////////
    public static list<Lead> createLead(Integer recordsToCreate) {

        list<Lead> l = new list<Lead>();
        for (Integer i=0; i<recordsToCreate; i++) {
            Lead LeadNew = new Lead();
            Schema.sObjectType.Lead.getRecordTypeInfosByName().get('Sample Request').getRecordTypeId();
            LeadNew.lastName = 'Lead - ' + i;
            LeadNew.Phone = '555-555-5555';
            LeadNew.Email = 'newlead@example.com';
            leadNew.Status = 'Open';
            LeadNew.Opportunity_Amount__c = 1;
            LeadNew.Country = 'United States';
            LeadNew.Company = 'Test Company - ' + i;
            LeadNew.Generalist_Email__c = 'newlead@example.com';
            LeadNew.Requestor_Email_LSS__c = 'newlead@example.com';
            l.add(LeadNew);
        }
        insert l;
        return l;
    }
    
    ////////////////////////////////////////////////////////////////////////////////
    //create test Product
    ////////////////////////////////////////////////////////////////////////////////
    public static list<Product2> createProduct(Integer recordsToCreate) {

        list<Product2> Prod2 = new list<Product2>();
        for (Integer i=0; i<recordsToCreate; i++) {
            Product2 ProdNew = new Product2();
            ProdNew.Isactive = True;
            ProdNew.Name = 'Product - ' + i;
            ProdNew.Catalog_QUANTA__c = '22222 - 00' + i;
            ProdNew.Product_Sales_Type__c = 'Regular Product';
            ProdNew.ProductCode = 'CustomProduct';
            Prod2.add(ProdNew);
        }
        insert Prod2;
        return Prod2;
        
        
    } 
    
    ////////////////////////////////////////////////////////////////////////////////
    //create test Ordered Sample Product
    ////////////////////////////////////////////////////////////////////////////////
    public static Ordered_Sample_Product__c createOSP(Id leadId, id productId) {
        Ordered_Sample_Product__c NewOSP = new Ordered_Sample_Product__c();
        NewOSP.Lead__c = leadId;
        NewOSP.Product__c = productId;
        insert NewOSP;
        return NewOSP;
        }
        
}