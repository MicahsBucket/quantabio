/**
 * Created by oliverpreuschl on 17.03.16.
 */

public with sharing class FivePrime_TopContent {

    private Settings__c GO_Settings{
        get{
            if( GO_Settings == null ){
                GO_Settings = Settings__c.getInstance();
            }
            return GO_Settings;
        }
        set;
    }

    public List< ContentDocument > GL_Documents{
        get{
            if( GL_Documents == null ){
                if( GO_Settings != null && String.isNotBlank( GO_Settings.CommunityContentLibraryId__c ) ) {
                    GL_Documents = [ SELECT Id, Title, FileExtension, FileType, Description, PublishStatus, LatestPublishedVersionId, ContentModifiedDate, ArchivedDate, ParentId FROM ContentDocument WHERE ( ParentId = :GO_Settings.CommunityContentLibraryId__c ) ORDER BY ContentModifiedDate DESC ];
                }else{
                    GL_Documents = new List< ContentDocument >();
                }
            }
            System.debug( 'GL_Documents: ' + GL_Documents );
            return GL_Documents;
        }
        set;
    }

    public FivePrime_TopContent(){
    }

}