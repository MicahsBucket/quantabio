public without sharing class MaterialOrderLogic {
/**
*   {Purpose}  –  This class performs various actions against the Material Order object
*                
*   {Function}  – 
*                 
*   {Support}   - For assistance with this code, please contact support@demandchainsystems.com             
*                 www.demandchainsystems.com
*                 (952) 345-4533
*/
/**
*   CHANGE  HISTORY
*   =============================================================================
*   Date      Name             Description
*   20181115  EBG DCS          Created
*   =============================================================================
*/    
	///////////////////////////////////////////////////////////////////////////
    //retrieve materials from a kit and associated to the orders
	///////////////////////////////////////////////////////////////////////////
    public static void createOrderMaterials(Map<Id, Office_Order__c> mapNewOrders, Map<Id, Office_Order__c> mapOldOrders) {
        
		Set<Id> setOrdersToProcess = new Set<Id>();
        Set<Id> setDeleteOrderMaterials = new Set<Id>();
        Set<Id> setKitIds = new Set<Id>();
        for (Office_Order__c oOrder : mapNewOrders.values()) {
            if (oOrder.Marketing_Kit__c != null &&
                    (mapOldOrders == null
                        || oOrder.Marketing_Kit__c != mapOldOrders.get(oOrder.Id).Marketing_Kit__c)) {
                setOrdersToProcess.add(oOrder.Id);                
                setKitIds.add(oOrder.Marketing_Kit__c);
            }
            
            //determine if an order needs its kit materials deleted
            if (mapOldOrders != null
                	&& oOrder.Marketing_Kit__c != mapOldOrders.get(oOrder.Id).Marketing_Kit__c) {
				setDeleteOrderMaterials.add(oOrder.Id);
			}
        }
        
        //delete kit materials
        if (setDeleteOrderMaterials.size() > 0) {
            MaterialOrderLogic.deleteKitMaterials(setDeleteOrderMaterials);
        }
        
        //get the marketings kits
        Map<Id, List<Marketing_Kit_Material__c>> mapKitMaterials = new Map<Id, List<Marketing_Kit_Material__c>>();
        for (Marketing_Kit__c oKit: [Select Id
                                    		, Name
                                     		, Location__c
                                    		, (Select Id
                                                    , Name
                                                    , Quantity__c
                                                    , Marketing_Material__c
                                                    , Marketing_Material__r.Name
                                                    , Marketing_Kit__r.Location__c
                                               		, Marketing_Kit__r.Stock_Field__c
                                                    , Marketing_Material__r.Active__c
                                                    , Marketing_Material__r.Availability__c
                                                    , Marketing_Material__r.In_Stock_Beverly__c
                                                    , Marketing_Material__r.In_Stock_Hilden__c
                                                    , Marketing_Material__r.Quantity_Available__c
                                                    , Marketing_Material__r.Ref_ID__c
                                                    , Marketing_Material__r.Type__c
                                               From Marketing_Kit_Materials__r)                                               
                                    From Marketing_Kit__c
									Where Id in :setKitIds]) {
			mapKitMaterials.put(oKit.Id, oKit.Marketing_Kit_Materials__r);
		}

        //loop through the order and add line items based on the associated kit
        List<Materials__c> lstMaterials = new List<Materials__c>();
        for (Office_Order__c oOrder : mapNewOrders.values()) {
            if (setOrdersToProcess.contains(oOrder.Id)
                	&& mapKitMaterials.containsKey(oOrder.Marketing_Kit__c)) {
				for (Marketing_Kit_Material__c oKitMaterial : mapKitMaterials.get(oOrder.Marketing_Kit__c)) {
                    Materials__c oNewMaterial = new Materials__c(Office_Order__c = oOrder.Id
                                                                 , Quantity__c = oKitMaterial.Quantity__c
                                                                 , Marketing_Material_Order__c = oKitMaterial.Marketing_Material__c
                                                                 , Marketing_Kit_Material__c = oKitMaterial.Id
                                                                 , Stock_Field__c = oKitMaterial.Marketing_Kit__r.Stock_Field__c);
					lstMaterials.add(oNewMaterial);
                }
            }
        }
        
        if (lstMaterials.size() > 0) {
            insert lstMaterials;
        }        
    }
    
	///////////////////////////////////////////////////////////////////////////
    //delete materials under the given orders if they had been added from a kit
	///////////////////////////////////////////////////////////////////////////
    public static void deleteKitMaterials(Set<Id> setOrderIds) {
        system.debug('Inside deleteKitMaterials: ' + setOrderIds);
        List<Materials__c> lstDeleteMaterials = new List<Materials__c>();
        for (Materials__c oMaterial : [Select Id 
										From Materials__c 
										Where Office_Order__c in :setOrderIds 
											And Marketing_Kit_Material__c != null]) {
			lstDeleteMaterials.add(oMaterial);
		}
        
        if (lstDeleteMaterials.size() > 0) {
            delete lstDeleteMaterials;
        }
    }
    
	///////////////////////////////////////////////////////////////////////////
    //description field is pre-populated via a process builder and hard returns
    // sometimes are displayed as the text '_BR_ENCODED_'
	///////////////////////////////////////////////////////////////////////////
    public static void cleanDescription (List<Office_Order__c> lstOrders) {
        for (Office_Order__c oOrder : lstOrders) {
            if (oOrder.Description__c != null) {
            	oOrder.Description__c = oOrder.Description__c.replace('_BR_ENCODED_', '\n');
            }
        }
    }

	///////////////////////////////////////////////////////////////////////////
    //Adjust quantity on Marketing Material records when items are added, updated,
    // or removed from an order
	///////////////////////////////////////////////////////////////////////////
    public static void adjustMaterialQuantity(Map<Id, Materials__c> mapMaterialsNew, Map<Id, Materials__c> mapMaterialsOld) {
        Map<Id, Map<String, Decimal>> mapMaterialAdjustment = new Map<Id, Map<String, Decimal>>();
        Decimal qty = 0;
        Map<String, Decimal> mapFieldQty = new Map<String, Decimal>();
        
        if (mapMaterialsNew == null) {
            //handles deletes - increase quantity
            for (Materials__c oDeletedMaterial : mapMaterialsOld.values()) {
                qty = 0;
                mapFieldQty = new Map<String, Decimal>();
                if (oDeletedMaterial.Marketing_Material_Order__c != null && oDeletedMaterial.Quantity__c != null) {
                    if (mapMaterialAdjustment.containsKey(oDeletedMaterial.Marketing_Material_Order__c)) {
                        mapFieldQty = mapMaterialAdjustment.get(oDeletedMaterial.Marketing_Material_Order__c);
                        if (mapFieldQty.containsKey(oDeletedMaterial.Stock_Field__c)) {
							qty = mapFieldQty.get(oDeletedMaterial.Stock_Field__c);
                            system.debug('Delete - Existing value ' + oDeletedMaterial.Stock_Field__c + ': ' + qty);
                        }
                    }
                    system.debug('Delete prep.  Qty: ' + qty + '  oDeletedMaterial.Quantity__c: ' + oDeletedMaterial.Quantity__c);
                    qty = qty + oDeletedMaterial.Quantity__c;
                    mapFieldQty.put(oDeletedMaterial.Stock_Field__c, qty);
                    system.debug(oDeletedMaterial.Marketing_Material_Order__c + ' - Delete - mapFieldQty: ' + mapFieldQty);
                    mapMaterialAdjustment.put(oDeletedMaterial.Marketing_Material_Order__c, mapFieldQty);
                }
            }
        } else {
            for (Materials__c oOrderMaterial : mapMaterialsNew.values()) {
                qty = 0;
                mapFieldQty = new Map<String, Decimal>();
                if (oOrderMaterial.Marketing_Material_Order__c != null && oOrderMaterial.Quantity__c != null) {
                    if (mapMaterialAdjustment.containsKey(oOrderMaterial.Marketing_Material_Order__c)) {
                        mapFieldQty = mapMaterialAdjustment.get(oOrderMaterial.Marketing_Material_Order__c);
                        if (mapFieldQty.containsKey(oOrderMaterial.Stock_Field__c)) {
							qty = mapFieldQty.get(oOrderMaterial.Stock_Field__c);
                            system.debug('Update - Existing value ' + oOrderMaterial.Stock_Field__c + ': ' + qty);
                        }
                    }
                    if (mapMaterialsOld == null) {
                        //handles inserts - reduce quantity
	                    system.debug('Insert prep.  Qty: ' + qty + '  oOrderMaterial.Quantity__c: ' + oOrderMaterial.Quantity__c);
                        qty = qty - oOrderMaterial.Quantity__c;
                    } else {
                        //handles adjustment
	                    system.debug('Update prep.  Qty: ' + qty + '  oOrderMaterial.Quantity__c: ' + oOrderMaterial.Quantity__c + '  Old Value: ' + mapMaterialsOld.get(oOrderMaterial.Id).Quantity__c);
                        qty = qty - oOrderMaterial.Quantity__c + mapMaterialsOld.get(oOrderMaterial.Id).Quantity__c;
                    }
                    mapFieldQty.put(oOrderMaterial.Stock_Field__c, qty);
                    system.debug(oOrderMaterial.Marketing_Material_Order__c + ' - Insert/Update - mapFieldQty: ' + mapFieldQty);
                    mapMaterialAdjustment.put(oOrderMaterial.Marketing_Material_Order__c, mapFieldQty);
                }
            }
        }
        
        //Locate the materials and adjust the stock by looking at the Stock Field value
        List<Marketing_Material__c> lstMaterialUpdates = new List<Marketing_Material__c>();
		for (Marketing_Material__c oMaterial : [Select Id
                                                		, Name
                                                		, In_Stock_Beverly__c
                                                		, In_Stock_Hilden__c 
                                                From Marketing_Material__c 
                                                Where Id in :mapMaterialAdjustment.keySet()]){
			system.debug('oMaterial: ' + oMaterial);
			Map<String, Decimal> mapFieldValue = mapMaterialAdjustment.get(oMaterial.Id);
			system.debug('mapFieldValue: ' + mapFieldValue);
			for (String fieldName : mapFieldValue.keySet()) {
                switch on fieldName {
                    when 'In_Stock_Beverly__c' {
                        system.debug('Updating Beverly.  oMaterial.In_Stock_Beverly__c: ' + oMaterial.In_Stock_Beverly__c + ' Amount to adjust: ' + mapFieldValue.get(fieldName));
                    	oMaterial.In_Stock_Beverly__c = oMaterial.In_Stock_Beverly__c + mapFieldValue.get(fieldName);
                	}
                    when 'In_Stock_Hilden__c' {
                        system.debug('Updating Hilden.  oMaterial.In_Stock_Hilden__c: ' + oMaterial.In_Stock_Hilden__c + ' Amount to adjust: ' + mapFieldValue.get(fieldName));
                    	oMaterial.In_Stock_Hilden__c = oMaterial.In_Stock_Hilden__c + mapFieldValue.get(fieldName);
                    }
                }
			}
			lstMaterialUpdates.add(oMaterial);
		}
        
        if (lstMaterialUpdates.size() > 0) {
            update lstMaterialUpdates;
        }
    }
    
}