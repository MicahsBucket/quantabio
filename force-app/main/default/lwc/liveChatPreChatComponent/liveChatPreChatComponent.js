import BasePrechat from 'lightningsnapin/basePrechat';
import { api, track, wire } from 'lwc';
import startChatLabel from '@salesforce/label/c.StartChat';
import { getPicklistValues } from 'lightning/uiObjectInfoApi';
import COUNTRY_FIELD from '@salesforce/schema/Account.ShippingCountryCode';
import getStates from '@salesforce/apex/DynamicPicklistUtility.getStates';
import getProvinces from '@salesforce/apex/DynamicPicklistUtility.getProvinces';

export default class liveChatPreChatComponent extends BasePrechat {
    @api prechatFields;
    @api backgroundImgURL;
    @track fields;
    @track namelist;
    @track options;
    // @track optionslist;
    @track optionsStatelist;
    optionslist = [{label:'--None--', value:'--None--'},
    {label:'Australia', value:'Australia'}, 
    {label:'Canada', value:'Canada'}, 
    {label:'France', value:'France'}, 
    {label:'Germany', value:'Germany'}, 
    {label:'Italy', value:'Italy'}, 
    {label:'United Kingdom', value:'United Kingdom'}, 
    {label:'United States', value:'United States'},    
    {label:'-------------', value:'-----------'},   
    ];
    @wire(getPicklistValues, { recordTypeId: '012000000000000AAA', fieldApiName: COUNTRY_FIELD})
    wiredPicklist({ error, data}){
        if(data){
            console.log('picklistdata' , data);
          
            data.values.forEach(key => {
                if(key.label != 'Australia' &&
                    key.label != 'Canada' &&
                    key.label != 'France' &&
                    key.label != 'Germany' &&
                    key.label != 'Italy' &&
                    key.label != 'United Kingdom' &&
                    key.label != 'United States' 
                ){
                    this.optionslist.push({
                        label : key.label,
                        value: key.label
                    })
                }
                
            });
            console.log('this.optionslist', this.optionslist);
        }
        else if(error){
            console.log(error);
        }
    }
    
    startChatLabel;
    chatType = '';
    PreChatOpen = true;
    orderTypeSelected = false;
    skillId = '';
    showTechnicalButton = false;
    showSampleButton = false;
    showProductButton = false;
    showCustomerServiceButton = false;
    showOtherButton = false;
    errorMessage = false;
    ErrorMessageText = 'Required Fields: ';
    container;
    showStateProvince = false;
    FirstNameError = false;
    LastNameError = false;
    EmailError = false;
    PhoneError = false;
    Country__cError = false;
    State_Province__cError = false;
    SubjectError = false;
    Request_Type__cError = false;
    errorField;
    optionsStateProvincelist = [];

    connectedCallback() {
        this.startChatLabel = startChatLabel;
        this.fields = this.prechatFields.map(field => {
            const { label, name, value, required, maxLength } = field;
            return { label, value, name, required, maxLength };
            
        });
        this.namelist = this.fields.map(field => field.name);
        console.log('this.namelist==== ', this.namelist);
        
    }

    /**
     * Focus on the first input after this component renders.
     */
    // renderedCallback() {
    //     console.log(document.getElementsByClassName('State_Province__c'));
    // }

    /**
     * On clicking the 'Start Chatting' button, send a chat request.
     */
    handleStartChat(event) {
        let isAllFieldsEntered = true;
        this.template.querySelectorAll("lightning-input").forEach(input => {
            this.fields[this.namelist.indexOf(input.name)].value = input.value;
        });
        this.fields[this.namelist.indexOf('LeadSource')].value = 'Chat Website';
        this.ErrorMessageText = 'Required Fields: ';
        if (this.validateFields(this.fields).valid) {
            for (let i = 0; i < this.fields.length; i++) {
                console.log('this.fields==== ',this.fields[i])
                if((this.fields[i].value == '' || this.fields[i].value == null ||this.fields[i].value == undefined) && this.fields[i].required == true){
                    isAllFieldsEntered = false
                    this.ErrorMessageText += this.fields[i].label + ', ';
                }
                
            }
            this.ErrorMessageText = this.ErrorMessageText.replace(/,\s*$/, "");
            if(isAllFieldsEntered == true){
                this.startChat( this.fields);
                
            }
            else{
                this.errorMessage = true;
            }
            
        } else {
            console.log(error);
        }
    }

    /// ChatType
    handleClick(event){
        this.chatType = event.target.label;
        this.skillId = event.target.value;
        this.fields[this.namelist.indexOf('Lead_Type__c')].value = this.chatType;
        this.orderTypeSelected = true;
        this.PreChatOpen = false;
    }

    addStateProvince(event){
        this.fields[this.namelist.indexOf('State__c')].value = event.target.value;
    }

    handleCountry(event){
        console.log('Name = ', event.target.name);
        console.log('Value = ', event.target.value);
        this.fields[this.namelist.indexOf('Country__c')].value = event.target.value;
        if(event.target.value == 'United States'){
            console.log('In USA');
            getStates()
            .then(result => {
                this.showStateProvince = true;
                this.optionsStateProvincelist = result;
                this.fields[this.namelist.indexOf('State__c')].required = true;
            })
            .catch(error => {
                this.optionsStateProvincelist = error;
            });
        }
        if(event.target.value == 'Canada'){
            console.log('In Canada');
            getProvinces()
            .then(result => {
                this.showStateProvince = true;
                this.optionsStateProvincelist = result;
                this.fields[this.namelist.indexOf('State__c')].required = true;
            })
            .catch(error => {
                this.optionsStateProvincelist = error;
            });
        }
    }
}
