trigger trgMaterialOrder on Office_Order__c (before insert, before update, after insert, after update) {
/**
*   {Purpose}  –  This triggers performs various actions against the Material Order object
*                
*   {Function}  – 
*                 
*   {Support}   - For assistance with this code, please contact support@demandchainsystems.com             
*                 www.demandchainsystems.com
*                 (952) 345-4533
*/
/**
*   CHANGE  HISTORY
*   =============================================================================
*   Date      Name             Description
*   20181115  EBG DCS          Created
*   =============================================================================
*/ 

    if (trigger.isBefore) {
		//clean up description populated via process builder
		MaterialOrderLogic.cleanDescription(trigger.new);
    }
    if (trigger.isAfter) {
        //create order materials for the orders based on the associated kits
        MaterialOrderLogic.createOrderMaterials(trigger.newMap, trigger.oldMap);        
    }
}