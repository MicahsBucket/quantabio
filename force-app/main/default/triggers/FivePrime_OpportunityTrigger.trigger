/**
 * Created by oliverpreuschl on 11.03.16.
 */

trigger FivePrime_OpportunityTrigger on Opportunity ( after update ) {

    if( Trigger.isAfter ){
        if( Trigger.isUpdate ){
            FivePrime_OpportunityTriggerHandler.handleAfterUpdate( Trigger.oldMap, Trigger.newMap );
        }
    }

}