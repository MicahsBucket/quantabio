trigger AgentWorkTrigger on AgentWork (after insert) {
    system.debug(Trigger.new);
    Id QueueId;
    Id notificationId;
    Organization o = [SELECT Id, InstanceName, IsSandbox, Name, OrganizationType FROM Organization];
    if(Test.isRunningTest()){
        if(o.IsSandbox == true){
            QueueId = '00G3J000000aCXRUA2';
        }
        else{
            QueueId = '00G4O000003dM0uUAE';
        }
    }
    else{
        QueueId = Trigger.new[0].OriginalQueueId;
    }
    Group g = [SELECT Id, Name, RelatedId, QueueRoutingConfigId, OwnerId FROM Group WHERE Id =: QueueId];
    List<GroupMember> gmList = [SELECT UserOrGroupId, GroupId FROM GroupMember WHERE GroupId =:g.Id];
    set<Id> UserIdsToEmail = new set<Id>();
    for(GroupMember gm: gmList){
        UserIdsToEmail.add(gm.UserOrGroupId);
    }
    list<UserServicePresence> uspList = [SELECT Id, UserId, User.email, StatusStartDate, StatusEndDate FROM UserServicePresence WHERE UserId IN: UserIdsToEmail AND StatusEndDate = null];
    System.debug('UserServicePresence '+ uspList);
    String[] toAddresses = new String[] {}; 
    set<String> idsToNotify = new set<String>();
    for(UserServicePresence u: uspList){
            toAddresses.add(u.User.Email);
            idsToNotify.add(String.valueOf(u.UserId));
    }

   if(toAddresses.size() > 0){
        Messaging.reserveSingleEmailCapacity(1);
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(toAddresses);
        mail.setReplyTo('sales@quantabio.com');
        mail.setSenderDisplayName('Salesforce Automated Chat Alert');
        mail.setSubject('New ' + g.Name + ' Chat');
        mail.setBccSender(false);
        mail.setUseSignature(false);
        mail.setHtmlBody('<p>You have a new chat in your '+ g.Name +' Que!</p>');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
   }

   Messaging.CustomNotification notification = new Messaging.CustomNotification();
    notification.setBody('New '+g.name + ' chat!');
    notification.setTitle('New '+g.name + ' chat! Please Navigate to Omni-Channel as soon as you are available!');
    notification.setSenderId(g.OwnerId);
    if(o.IsSandbox == true){
        notificationId = '0ML3J0000004CezWAE';
    }
    else{
        notificationId = '0ML4O0000004C9mWAE';
    }
    notification.setNotificationTypeId(notificationId);
    notification.setTargetId(Trigger.new[0].WorkItemId); // target object id
    notification.send(idsToNotify); // target user id.
}