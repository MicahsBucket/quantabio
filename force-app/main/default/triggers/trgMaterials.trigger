trigger trgMaterials on Materials__c (after insert, after update, after delete) {
/**
*   {Purpose}  –  This triggers performs various actions against the Materials object
*                
*   {Function}  – 
*                 
*   {Support}   - For assistance with this code, please contact support@demandchainsystems.com             
*                 www.demandchainsystems.com
*                 (952) 345-4533
*/
/**
*   CHANGE  HISTORY
*   =============================================================================
*   Date      Name             Description
*   20181127  EBG DCS          Created
*   =============================================================================
*/ 

    if (trigger.isAfter && trigger.isInsert) {
        MaterialOrderLogic.adjustMaterialQuantity(trigger.newMap, null);        
    }
    if (trigger.isAfter && trigger.isUpdate) {
        MaterialOrderLogic.adjustMaterialQuantity(trigger.newMap, trigger.oldMap);        
    }
    if (trigger.isAfter && trigger.isDelete) {
        MaterialOrderLogic.adjustMaterialQuantity(null, trigger.oldMap);        
    }
}