trigger Lead_OSP_Count on Ordered_Sample_Product__c (after update, after insert, after delete, after undelete) {

  Set<Id> setLeadids = new Set<Id>();
  
  //Whenever your working with After Undelete operation you can access data through 
  //Trigger.new or Trigger.newMap but not with Trigger.old or Trigger.oldmap variables
  if(Trigger.isInsert || Trigger.isUndelete || Trigger.isUpdate)
  {
   for(Ordered_Sample_Product__c osp : Trigger.new)
   {
    setLeadids.add(osp.lead__c);
   }
  }
  
  if(Trigger.isDelete)
  {
   //if you use Trigger.new below in place of Trigger.old you will end up with 
   //System.NullPointerException:Attempt to de-reference a null object
   for(Ordered_Sample_Product__c osp : Trigger.old) 
   {
    setLeadids.add(osp.lead__c);
   }
  }
  
 List<Lead> listl = [Select id, number_of_OSP__c, Custom_Sample__c, (select id, product__r.isactive, product__r.available_for_sample_request__c, product__r.name, product__r.productcode, lead__c from lead_samples__r) 
                    from Lead where Id in : setLeadids];
  for(Lead l :listl)
  {
   l.number_of_OSP__c = l.lead_samples__r.size();
   l.custom_sample__c = false;
   for(Ordered_Sample_Product__c osp : l.lead_samples__r){
       if (osp.product__r.isactive && 
            (
              osp.product__r.productcode == 'CustomProduct'||
              osp.product__r.productcode == '95900-2C'||
              osp.product__r.productcode == '95900-4C'
             )
          ){
           l.custom_sample__c = true;
           break;
           }
   }
  }
  update listl;
}