trigger LeadTrigger on Lead (before insert, after update) {
    if(Trigger.isBefore && Trigger.isInsert){
        for(Lead l: Trigger.new){
            System.debug('in trigger');
            if(l.Lead_Type__c != null){
                l.Note__c = 'Chat Request for: ' + l.Lead_Type__c;
                l.RecordTypeId = [SELECT Id, Name, SobjectType FROM RecordType WHERE Name = 'Sample Request' AND SobjectType = 'Lead'].Id;
            }
        }
    }
}